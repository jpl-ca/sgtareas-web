<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

use IrisGPS\Contracts\Trackable\TrackableContract;

use IrisGPS\Scopes\OrganizationScope;

use IrisGPS\Trackable\Trackable as TrackableTrait;

use IrisGPS\TrackableType;

use Carbon\Carbon;

use Exception;

class Agent extends Model implements TrackableContract
{
    use TrackableTrait;

    public function __construct()
    {
        parent::__construct();
        static::addGlobalScope(new OrganizationScope);
        $this->initialize();
        $this->trackableType = 'agent';
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function listVisitPointsByDay($constraints = [])
    {
        $visitPoints = TasksVisitPoint::join('tasks', 'tasks.id', '=', 'tasks_visit_points.task_id');
        $visitPoints->where('tasks.organization_id', $this->organization_id)
                    ->where('tasks.taskable_id', $this->id)
                    ->where('tasks.taskable_type', TrackableType::AGENT);

        if (isset($constraints['startDate'])) {
            $visitPoints->where('datetime', '>', $constraints['startDate']);
        }

        if (isset($constraints['endDate'])) {
            $date = Carbon::parse($constraints['endDate']);
            $date = $date->addDays(1);
            $visitPoints->where('datetime', '<', $date);
        }

        if (isset($constraints['statuses'])) {
            $visitPoints->whereIn('visit_state_id', $constraints['statuses']);
        }

        $result = [
            'days' => []
        ];

        $items = $visitPoints->get();
        foreach ($items as $item) {
            if (isset($item->datetime)) {
                $date = Carbon::parse($item->datetime)->toDateString();
                $result['days'][$date][] = $item;    
            }
        }

        return $result;
    }

    public function geolocationHistorySessions($options)
    {
        $secondsInSession = 120;
        $options['trackableId'] = $this->trackable->id;

        if (!isset($options['trackableId'])) {
            throw new Exception("trackableId is not present");
        }

        $trackableId = $options['trackableId'];
        $totalDistance = (double) 0.000000000;
        
        $items = GeolocationHistory::where('trackable_id', $trackableId);

        $options['startDate'] = isset($options['startDate']) ? trim($options['startDate']) : null;
        $options['endDate'] = isset($options['endDate']) ? trim($options['endDate']) : null;

        if (!empty($options['startDate'])) {
            $startDate = Carbon::parse($options['startDate'])->toDateTimeString();
            $items = $items->where('created_at', '>=', $startDate);
        }

        if (!empty($options['endDate'])) {
            if (strlen($options['endDate']) == 10) {
                $endDate = Carbon::parse($options['endDate'])->addDays(1)->addSeconds(-1)->toDateTimeString();
            } else {
                $endDate = Carbon::parse($options['endDate'])->toDateTimeString();
            }
            
            $items = $items->where('created_at', '<=', $endDate);
        }

        $rows = $items->where('trackable_id', $trackableId)->get()->toArray();
        $sections = [];
        $indexOfSection = 0;
        $output = "";
        $i = 0;

        while($i < count($rows) - 1) {
            $pointA = $rows[$i];
            $pointB = $rows[$i+1];
            $dateTimeOfPointA = Carbon::parse($pointA['created_at']);
            $dateTimeOfPointB = Carbon::parse($pointB['created_at']);
            $diffInSeconds = abs($dateTimeOfPointA->diffInSeconds($dateTimeOfPointB));

            if ($diffInSeconds > $secondsInSession) {
                $sections[$indexOfSection][] = $pointA;
                $indexOfSection++;
            } else {
                $sections[$indexOfSection][] = $pointA;
            }

            $i++;
        }

        foreach ($sections as $sectionKey => $section) {
            $items = $section;
            if (count($items) >= 2) {
                for ($i = 0;$i < count($items) - 1; $i++) {
                    $startPoint = $items[$i];
                    $endPoint = $items[$i+1];
                    $distance = distanceBetweenPoints(floatval($startPoint['lat']), floatval($startPoint['lng']), floatval($endPoint['lat']), floatval($endPoint['lng']), 'K');

                    $distance = is_nan($distance) ? 0 : $distance;
                    $totalDistance += $distance;
                }
            }
        }

        return $sections;
    }

    public function snappedGeolocationHistorySessions($options)
    {
        $tempPoints = [];
        $newSections = [];
        $options['secondsInSession'] = 9999999999999;
        $sections = $this->cleanGeolocationHistorySessions($options);
        if (end($sections)) {
            $array = array_slice(end($sections), -100, 100, true);
            foreach ($array as $it) {
                $strPoint = $it['lat'] . ', ' . $it['lng'];
                $tempPoints[]= $strPoint;
            }
        }

        //$url = "https://roads.googleapis.com/v1/snapToRoads?path=" + join('|', $tempPoints) + "&interpolate=true&key=AIzaSyCcurOu254Rjz88Zh7deRelYNd-4zB2NFk";
        try {
            $url = "https://roads.googleapis.com/v1/snapToRoads?path=" . join('|', $tempPoints) . "&interpolate=true&key=AIzaSyCcurOu254Rjz88Zh7deRelYNd-4zB2NFk";
            //$url = "https://roads.googleapis.com/v1/snapToRoads?path=&interpolate=true&key=AIzaSyCcurOu254Rjz88Zh7deRelYNd-4zB2NFk";
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $url);
            $json = json_decode($res->getBody(), true);
            $newPoints = [];
            if (!empty($json['snappedPoints'])) {
                foreach ($json['snappedPoints'] as $key => $value) {
                    $newPoint['lat'] = $value['location']['latitude'];
                    $newPoint['lng'] = $value['location']['longitude'];
                    $newPoints[] =$newPoint;
                }

                $newSections[0] = $newPoints;
            }
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }

        return $newSections;
    }

    public function cleanGeolocationHistorySessions($options)
    {
        $secondsInSession = 99999999999;
        $options['trackableId'] = $this->trackable->id;

        if (!isset($options['trackableId'])) {
            throw new Exception("trackableId is not present");
        }

        $trackableId = $options['trackableId'];
        $totalDistance = (double) 0.000000000;
        
        $items = GeolocationHistory::where('trackable_id', $trackableId);

        $options['startDate'] = isset($options['startDate']) ? trim($options['startDate']) : null;
        $options['endDate'] = isset($options['endDate']) ? trim($options['endDate']) : null;

        if (!empty($options['startDate'])) {
            $startDate = Carbon::parse($options['startDate'])->toDateTimeString();
            $items = $items->where('created_at', '>=', $startDate);
        }

        if (isset($options['secondsInSession'])) {
            $secondsInSession = (int) $options['secondsInSession'];
        }

        if (!empty($options['endDate'])) {

            if (strlen($options['endDate']) == 10) {
                $endDate = Carbon::parse($options['endDate'])->addDays(1)->addSeconds(-1)->toDateTimeString();
            } else {
                $endDate = Carbon::parse($options['endDate'])->toDateTimeString();
            }
            
            $items = $items->where('created_at', '<=', $endDate);
        }

        $items = $items->orderBy('created_at', 'asc');

        $rows = $items->where('trackable_id', $trackableId)->get()->toArray();
        $sections = [];
        $indexOfSection = 0;
        $output = "";
        $i = 0;

        while($i < count($rows) - 1) {
            $pointA = $rows[$i];
            $pointB = $rows[$i+1];
            $dateTimeOfPointA = Carbon::parse($pointA['created_at']);
            $dateTimeOfPointB = Carbon::parse($pointB['created_at']);
            $diffInSeconds = abs($dateTimeOfPointA->diffInSeconds($dateTimeOfPointB));
            $sections[$indexOfSection][] = $pointA;

            if ($i == count($rows) - 2) {
                $sections[$indexOfSection][] = $pointB;
            }
            $i++;
        }

        $deleted = 0;

        $times = 0;
        do {
            $times++;
            $sections = $this->calculateValuesForItems($sections);
            $changed = $this->cleanAcceleration($sections);
        } while ($changed || $times > 10);
        //dd($times);
        return $sections;
    }

    private function cleanAcceleration (& $sections) {
        //Cleaning noise
        $changed = false;
        for ($i = 0; $i < count($sections); $i++) {
            for ($j = 0;$j < count($sections[$i]); $j++) {                
                if (isset($sections[$i][$j]['acceleration']) && abs($sections[$i][$j]['acceleration']) > 3000) {
                    unset($sections[$i][$j]);
                    $changed = true;
                } elseif (isset($sections[$i][$j]['speed']) && $sections[$i][$j]['speed'] == 0) {
                    unset($sections[$i][$j]);
                    $changed = true;
                }
                /* elseif (!isset($sections[$i][$j]['acceleration']) && $j > 0) {
                    unset($sections[$i][$j]);
                    $changed = true;
                }*/
            }

            $sections[$i] = array_values($sections[$i]);
        }

        return $changed;
    }

    private function calculateValuesForItems(&$sections) {
        foreach ($sections as $sectionKey => $section) {
            $items = $section;
            if (count($items) >= 2) {

                for ($i = 0;$i < count($items) - 1; $i++) {
                    $startPoint = $items[$i];
                    $endPoint = $items[$i+1];
                    $distance = distanceBetweenPoints($startPoint['lat'], $startPoint['lng'], $endPoint['lat'], $endPoint['lng'], 'K');
                    $distance = is_nan($distance) ? 0 : $distance;
                    $dtStartPoint = Carbon::parse($startPoint['created_at']);
                    $dtEndPoint = Carbon::parse($endPoint['created_at']);
                    $diffInHours = (double) abs($dtStartPoint->diffInSeconds($dtEndPoint)) / 3600;

                    $distancePerHour = 0;
                    if ($diffInHours != 0) {
                        $distancePerHour = $distance / $diffInHours;
                    }

                    $sections[$sectionKey][$i + 1]['diffInHours'] = $diffInHours;
                    $sections[$sectionKey][$i + 1]['distance'] = $distance;
                    $sections[$sectionKey][$i + 1]['speed'] = $distancePerHour;
                    $sections[$sectionKey][$i + 1]['acceleration'] = null;

                    if (isset($sections[$sectionKey][$i + 1]['speed']) && isset($sections[$sectionKey][$i]['speed'])) {
                        $diffSpeed = $sections[$sectionKey][$i + 1]['speed'] - $sections[$sectionKey][$i]['speed'];
                        if ($diffInHours > 0) {
                            $acceleration = $diffSpeed / $diffInHours;
                            $sections[$sectionKey][$i + 1]['acceleration'] = $acceleration;
                        }
                    }
                }
            }
        }
        return $sections;
    }

    public function totalDistanceTraveled($options)
    {
        $totalDistance = 0;
        $sessions = $this->cleanGeolocationHistorySessions($options);
        foreach ($sessions as $session) {
            foreach ($session as $value) {
                if (isset($value['distance'])) {
                    $totalDistance += $value['distance'];
                }
            }
        }
        return $totalDistance;
    }
}
