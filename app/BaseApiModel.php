<?php
namespace IrisGPS;

use Exception;

Class BaseApiModel
{
    public static function searchItemsQuery($classReference, $options)
    {
        // Setting initial variables
        $startDate = isset($options['startDate']) ? $options['startDate'] : null;
        $endDate = isset($options['endDate']) ? $options['endDate'] : null;
        $searchTerm = isset($options['searchTerm']) ? $options['searchTerm'] : null;
        $searchType = isset($options['searchType']) ? $options['searchType'] : null;
        $filters = isset($options['filters']) ? $options['filters'] : null;

        //Building query
        $items = $classReference::query();
        if (isset($searchTerm) && isset($searchType)) {

            $items = $items->where(
                $searchType,
                'LIKE',
                '%' . $searchTerm . '%'
            );
        }

        if (isset($filters) && is_array($filters)) {
            foreach ($filters as $filter) {

                if ( isset($filter['field']) && isset($filter['value'])) {
                    if (isset($filter['condition'])) {
                        $items = $items->where($filter['field'], $filter['condition'], $filter['value']);
                    } else {
                        $items = $items->where($filter['field'], $filter['value']);
                    }
                } else {
                    throw new Exception("Error processing filter. field or value key is not present. These field are required.");
                }
            }
        }

        if (isset($startDate)) {
            $items = $items->where('created_at', '>=', $startDate);
        }

        if (isset($endDate)) {
            $items = $items->where('created_at', '<=', $endDate);
        }

        $items->orderBy('id', 'desc');

        //End building query
        return $items;
    }
}   
