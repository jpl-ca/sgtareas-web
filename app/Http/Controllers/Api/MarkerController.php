<?php

namespace IrisGPS\Http\Controllers\Api;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Http\Requests\StoreMarkerRequest;
use IrisGPS\Marker;

class MarkerController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('auth:web');
    }

    public function index($id = null)
    {
        if (is_null($id)) {
            $markers = auth()->user()->organization->markers;
            return response()->json($markers);
        }
        $marker = Marker::find($id);
        return response()->json($marker);
    }

    public function store(StoreMarkerRequest $request)
    {
        $marker = new Marker;

        $marker->name = $request->name;
        $marker->address = $request->address;
        $marker->phone = $request->phone;
        $marker->reference = $request->reference;
        $marker->lat = $request->latitude;
        $marker->lng = $request->longitude;

        $marker->save();

        return response()->json('Marker created successfully!.');
    }

    public function update(StoreMarkerRequest $request)
    {
        $marker = Marker::find($request->id);

        $marker->name = $request->name;
        $marker->address = $request->address;
        $marker->phone = $request->phone;
        $marker->reference = $request->reference;
        $marker->lat = $request->latitude;
        $marker->lng = $request->longitude;

        $marker->save();

        return response()->json('Marker updated successfully!.');
    }
}
