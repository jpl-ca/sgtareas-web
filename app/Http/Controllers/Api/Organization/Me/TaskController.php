<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\Task;

use Carbon\Carbon;

use Maatwebsite\Excel\Facades\Excel;

class TaskController extends Controller
{
    use BaseApiController;

    public function __construct()
    {
        $this->classReference = Task::class;
    }

    public function calendar(Request $request)
    {
        // Settings initial variables
        $agents = [];
        $visit_points = [];
        $format = $request->has('format') ? $request->format : 'json';

        // Fetching agents
        $items = $this->getCurrentOrganization()
                      ->agents()
                      ->get();

        // Building json format of agents
        foreach ($items as $item) {
            
            $agents[] = [
                'key' => 'IrisGPS\Agent-' . $item->id,
                'label' => $item->full_name
            ];
        }

        // Fetching visit points
        $items = $this->getCurrentOrganization()
                      ->visitPoints();

        if (isset($request->taskable_id)) {
            $items = $items->where('taskable_id', $request->taskable_id);
        }

        if (isset($request->startDate)) {
            $items = $items->where('tasks_visit_points.datetime', '>=', $request->startDate);
        }

        if (isset($request->endDate)) {
            $items = $items->where('tasks_visit_points.datetime', '<=', $request->endDate);
        }

        $items = $items->get();

        if (count($items) > 0) {

            foreach ($items as $item) {
                $task = $item->task;
                $pseudoIdOfTrackable = $task->taskable_type . '-' . $task->taskable_id;
                $agent_id = $task->taskable_id;
                $time = isset($item->datetime) ? Carbon::parse($item->datetime)->format('H:i') : null;
                $visit_points[] = [
                    'key' => $item->id,
                    'start_date' => Carbon::parse($item->datetime)->toDateTimeString(),
                    'end_date' => Carbon::parse($item->datetime)->toDateTimeString(),
                    'name' => $item->name,
                    'text' => $time . ' - ' . $item->name,
                    'lat' => $item->lat,
                    'lng' => $item->lng,
                    'checklist' => $item->checklist,
                    'trackable_id' => $pseudoIdOfTrackable,
                    'task_id' => $task->id,
                    'time' => $time,
                    'marker_id' => $item->marker_id,
                    'datetime' => $item->datetime,
                    'color' => "#" . $item->getHexadecimalColorAttribute()
                ];
            }    
        }
        


        switch ($format) {
            case 'json':
                // Building json format
                $json = [
                    'message' => 'Calendario cargado exitosamente',
                    'data' => [
                        'trackables' => $agents,
                        'visit_points' => $visit_points
                    ]
                ];

                return response()->json($json);

                break;

            case 'xls':

                $xls = Excel::create('Archivo', function($excel) use ($visit_points) {
                    $excel->sheet('Archivo', function($sheet) use ($visit_points) {
                        if (count($visit_points) > 0) {
                            $sheet->row(1, ['Fecha', 'Cliente', 'Tareas']);
                            $i = 2;
                            foreach ($visit_points as $key => $value) {
                                $visitPointDate = $value['datetime'];
                                $visitPointName = $value['name'];                                

                                $tasksNames = [];

                                foreach ($value['checklist'] as $item) {
                                    $tasksNames[] =$item['description'];
                                }

                                $visitPointTasks = join(', ', $tasksNames);

                                $sheet->row($i, [$visitPointDate, $visitPointName, $visitPointTasks]);
                                $i++;
                            }
                        }
                    });
                });

                $xls->download();

                break;
            default:
                return response('Formato no válido', 422);
                break;
        }
        
    }

    private function getCurrentOrganization()
    {
        return auth()->user()->organization;
    }
}
