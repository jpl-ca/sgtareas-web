<?php

namespace IrisGPS\Http\Controllers\Api;

use Illuminate\Http\Request;

use IrisGPS\Agent;

use IrisGPS\ChecklistItem;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Requests\StoreTaskRequest;

use IrisGPS\Task;

use IrisGPS\TasksVisitPoint;

use IrisGPS\VisitState;

use IrisGPS\Marker;

use Log;

use Validator;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('auth:web');
        $this->middleware('verify-privilege:view-task', ['only' => 'view']);
        $this->middleware('verify-privilege:create-task', ['only' => 'create']);
        $this->middleware('verify-privilege:store-task', ['only' => 'store']);
        $this->middleware('verify-privilege:edit-task', ['only' => ['edit', 'update'] ]);
        $this->middleware('verify-privilege:destroy-task', ['only' => 'destroy']);
    }

    public function index($id)
    {
        $task = Task::with('visitPoints.checklist')->find($id);

        return response()->json($task);
    }

    public function store(Request $request)
    {
        $task = new Task;

        $task->start_date = $request->start_date;
        $task->end_date = $request->end_date;
        $task->description = $request->description;

        list($type, $id) = explode('-', $request->assigned_to);

        $task->taskable_id = $id;
        $task->taskable_type = $type;

        $task->save();

        if (is_array($request->visit_list)) {
            foreach ($request->visit_list as $marker) {

                // Setting task visit point
                $taskVisitPoint = new TasksVisitPoint;
                if (isset($marker['lat'])) $taskVisitPoint->lat = $marker['lat'];
                if (isset($marker['lng'])) $taskVisitPoint->lng = $marker['lng'];
                if (isset($marker['name'])) $taskVisitPoint->name = $marker['name'];
                if (isset($marker['address'])) $taskVisitPoint->address = $marker['address'];
                if (isset($marker['phone'])) $taskVisitPoint->phone = $marker['phone'];
                if (isset($marker['reference'])) $taskVisitPoint->reference = $marker['reference'];
                if (isset($marker['id'])) $taskVisitPoint->marker_id = $marker['id'];
                $taskVisitPoint->task_id = $task->id;
                $taskVisitPoint->visit_state_id = VisitState::STATE_SCHEDULED;

                if (isset($marker['visit_point_id_parent'])) {
                    $taskVisitPoint->visit_point_id_parent = $marker['visit_point_id_parent'];
                }

                if (isset($marker['datetime'])) {
                    $taskVisitPoint->datetime = $marker['datetime'];
                }

                if (isset($marker['marker_id'])) {
                    $tempMarker = Marker::find($marker['marker_id']);
                    if ($tempMarker) {
                        $taskVisitPoint->lat = $tempMarker->lat;
                        $taskVisitPoint->lng = $tempMarker->lng;
                        $taskVisitPoint->address = $tempMarker->address;
                        $taskVisitPoint->phone = $tempMarker->phone;
                        $taskVisitPoint->reference = $tempMarker->reference;
                        $taskVisitPoint->marker_id = $tempMarker->id;
                    }
                }

                // Saving task
                $taskVisitPoint->save();

                // Creating checklist for visit point
                $checklist = [];

                if (isset($marker['checklist']) && is_array($marker['checklist'])) {
                    foreach ($marker['checklist'] as $item) {
                        $checkListItem = new ChecklistItem;
                        $checkListItem->description = $item['description'];
                        array_push($checklist, $checkListItem);
                    }    
                }
                

                $checklistItems = $taskVisitPoint->checklist()->saveMany($checklist);
            }
        }

        return response()->json($task);
    }

    public function update(Request $request)
    {
        $task = Task::findOrFail($request->id);

        $task->start_date = $request->start_date;
        $task->end_date = $request->end_date;
        $task->description = $request->description;
        list($type, $id) = explode('-', $request->assigned_to);
        $task->taskable_id = $id;
        $task->taskable_type = $type;
        $task->save();

        $visit_points = [];

        if($request->has('visit_list')) $visit_points = $request->visit_list;
        if($request->has('visit_points')) $visit_points = $request->visit_points;

        if (isset($visit_points) && is_array($visit_points)) {
            $task->saveVisitPoints($visit_points);
        }

        return response()->json($task);
    }

    public function saveVisitPoint(Request $request)
    {

        $visitPointId = $request->key;
        list($taskableType, $taskableId) = explode('-', $request->trackable_id);
        $markerId = $request->marker_id;
        $visitPointLat = $request->lat;
        $visitPointLng = $request->lng;
        $visitPointName = $request->name;
        $visitPointDateTime = $request->datetime;
        $visitPointCheckList = $request->checklist;

        if (isset($visitPointId)) {
            $visitPoint = TasksVisitPoint::find($visitPointId);
            if ($visitPoint->task) $lastTask = $visitPoint->task;
            
        } else {
            $visitPoint = new TasksVisitPoint;
        }

        // Setting task list
        $tasks = Task::where('start_date', '<=', $visitPointDateTime);
        $tasks->where('end_date', '>=', $visitPointDateTime);
        $tasks->where('taskable_type', $taskableType);
        $tasks->where('taskable_id', $taskableId);
        $tasks->where('organization_id', auth()->user()->organization_id);

        if ($tasks->count() > 0) {
            $task = $tasks->first();
        } else {
            $task = new Task;
        }

        if ($taskableType) $task->taskable_type = $taskableType;
        if ($taskableId) $task->taskable_id = $taskableId;

        $task->save();

        $visitPoint->marker_id = $markerId;
        $visitPoint->lat = $visitPointLat;
        $visitPoint->lng = $visitPointLng;
        $visitPoint->name = $visitPointName;
        $visitPoint->task_id = $task->id;
        $visitPoint->datetime = $visitPointDateTime;
        
        $visitPoint->save();

        // Saving visit points
        $existsChecklist = (isset($visitPointCheckList) && is_array($visitPointCheckList));

        if ($existsChecklist) {

            $checklist = [];
            $visitPoint->checklist()->delete();

            foreach ($visitPointCheckList as $list_it) {
                $checkListItem = new ChecklistItem;
                $checkListItem->description = $list_it['description'];
                array_push($checklist, $checkListItem);
            }

            $checklistItems = $visitPoint->checklist()->saveMany($checklist);    
        }

        if (isset($lastTask)) {
            $lastTask->deleteIfThisHasNotVisitPoints();
        }

        return response()->json([
            "message" => "Visit Point guardado",
            "data" => $visitPoint
        ]);
    }
}
