<?php

namespace IrisGPS\Http\Controllers;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

interface IWebController
{
    public function index(Request $request);
    public function create(Request $request);
    public function edit(Request $request);
    public function show(Request $request);
    public function update(Request $request);
    public function destroy(Request $request);
    
}
