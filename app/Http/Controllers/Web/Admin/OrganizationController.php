<?php

namespace IrisGPS\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Organization;

class OrganizationController extends Controller
{
    public function index(Request $request)
    {
    	
        $searchTypes = [
            'name' => 'Nombre',
            'subscription_id' => 'Tipo de suscripción'
        ];

        $searchText = $request->search_text ? $request->search_text : null;
        $searchType = $request->search_type ? $request->search_type : null;

        //Building Query
        $organizations = Organization::query();
        if ($searchType && $searchText) {
            $organizations = $organizations->where(
                $searchType,
                'LIKE',
                '%' . $searchText . '%'
            );
        }


    	//Building paginator
    	$organizations = $organizations->paginate();
    	$data = compact(
    		'organizations',
            'searchTypes',
            'searchText',
            'searchType'
    	);

    	return view('web.admin.organizations.index', $data);
    }

    public function view(Request $request, $id)
    {
        $organization = Organization::find($id);
        $data = compact(
            'organization'
        );

        $data['hasPrivilegesToEdit'] = true;
        $data['hasPrivilegesToDestroy'] = true;
        
        return view('web.admin.organizations.view',
            $data
        );
    }

    public function edit(Request $request, $id)
    {
        $organization = Organization::findOrFail($id);
        $typesOfSubscription = [
            'trial' => 'Trial',
            'basic' => 'Basic'
        ];

        $data = compact(
            'organization',
            'typesOfSubscription'
        );

        return view('web.admin.organizations.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $organization = Organization::findOrFail($id);

        $typesOfSubscription = [
            'trial' => 'Trial',
            'basic' => 'Basic'
        ];


        //Saving item
        if ($request->exists('ruc')) {
            $organization->ruc = $request->ruc;
        }

        if ($request->exists('name')) {
            $organization->name = $request->name;
        }

        if ($request->exists('subscription_id')) {
            $organization->subscription_id = $request->subscription_id;
        }

        $organization->save();

        $data = compact(
            'organization',
            'typesOfSubscription'
        );

        return view('web.admin.organizations.edit', $data);
    }

    public function destroy(Request $request, $id)
    {
        $organization = Organization::findOrFail($id);
        $organization->delete();

        return response()
            ->redirectToAction('Admin\OrganizationController@index');
    }

    public function lock(Request $request, $organization_id)
    {
        $organization = Organization::findOrFail($organization_id);
        $organization->locked = true;
        $organization->save();

        $response = response()
            ->redirectToAction('Web\Admin\OrganizationController@index',
                [
                    'organization_id' => $organization_id,
                    'format' => 'html'
                ])
            ->withSuccess('La organización fue bloqueada');

        return $response;
    }

    public function unlock(Request $request, $organization_id)
    {
        $organization = Organization::findOrFail($organization_id);
        $organization->locked = false;
        $organization->save();

        $response = response()
            ->redirectToAction('Web\Admin\OrganizationController@index',
                [
                    'organization_id' => $organization_id,
                    'format' => 'html'
                ])
            ->withSuccess('El organización fue desbloqueada');

        return $response;
    }
}
