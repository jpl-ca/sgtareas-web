<?php

namespace IrisGPS\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Suggestion;

class SuggestionController extends Controller
{
    public function index(Request $request)
    {
    	// Setting vars
    	$searchText = $request->has('search_text') ? $request->search_text : null;
    	$searchType = $request->has('search_type') ? $request->search_type : null;

    	$searchTypes = [
            'content' => 'Contenido'
        ];

    	//Building query
    	$items = Suggestion::query();
    	$items = $items->orderBy('created_at', 'desc');
    	if ($searchType && $searchText) {
    		$items = $items->where(
                $searchType,
                'LIKE',
                '%' . $searchText . '%'
            );
    	}

    	//Building paginator
    	$items = $items->paginate();

    	$data = compact(
    		'items',
    		'searchType',
    		'searchText',
    		'searchTypes'
    	);

    	return view('web.admin.suggestions.index', $data);
    }

    public function destroy(Request $request, $itemId)
    {
        $item = Suggestion::findOrFail($itemId);
        $item->delete();

        return response()
            ->redirectToAction('Web\Admin\SuggestionController@index')
            ->withSuccess('Eliminado exitosamente');
    }
}
