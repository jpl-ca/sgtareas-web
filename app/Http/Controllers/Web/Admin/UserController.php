<?php

namespace IrisGPS\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Requests\StoreUserRequest;
use IrisGPS\Http\Requests\UpdateOrDeleteUserRequest;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\User;
use IrisGPS\Privilege;

use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{

    public function __construct()
    {

    }

    public function index(Request $request, $format = 'html')
    {
        $users = User::sameOrganization();
        $searchText = $request->search_text ? $request->search_text : null;
        $searchType = $request->search_type ? $request->search_type : null;
        
        $searchTypes = array(
            'email' => 'E-mail',
            'name' => 'Nombre',
        );

        if ($searchType && $request->searchText) {
            $users = $users->where(
                $searchType,
                'LIKE',
                '%' . $searchText . '%'
            );
        }

        $users = $users->paginate(20);
        $hasPrivilegesToCreate = auth()->user()->hasPrivilege('create-user');
        $hasPrivilegesToEdit = auth()->user()->hasPrivilege('edit-user');
        $hasPrivilegesToDestroy = auth()->user()->hasPrivilege('destroy-user');

        // Formats
        switch ($format) {
            case 'xls':
                Excel::create('Usuarios', function($excel) use ($users) {
                    $excel->sheet('Usuarios', function($sheet) use ($users) {
                        $sheet->row(1, ['id','Nombre', 'E-mail']);
                        $i = 2;
                        foreach ($users as $key => $value) {
                            $sheet->row($i, [
                                $value->id,
                                $value->name,
                                $value->email
                                ]);
                            $i++;
                        }
                    });

                })->download();

                break;
            case 'html':
                return view('web.users.index',compact(
                        'users',
                        'hasPrivilegesToCreate',
                        'hasPrivilegesToEdit',
                        'hasPrivilegesToDestroy',
                        'searchTypes',
                        'searchType',
                        'searchText'
                    ));
                break;
        }
        
    }

    public function create()
    {
        return view('web.admin.users.create');
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()
            ->redirectToAction('Web\Admin\UserController@index')
            ->withSuccess('Usuario creado exitosamente.');
    }

    public function view(User $user)
    {
        $hasPrivilegesToEdit = true;
        $data = compact(
            'user',
            'hasPrivilegesToEdit'
        );
        return view('web.admin.users.view')->with($data);
    }

    public function edit(User $user)
    {
        $privileges = Privilege::all();

        return view('web.admin.users.edit')->with(
            compact('user','hasPermissionsToEdit', 'privileges')
            );
    }

    public function update(Request $request, User $user)
    {
        $nextUrl = ($request->next_url ? $request->next_url : null);

        if ($request->exists('name')) {
            $user->name = $request->name;    
        }
        
        if ($request->exists('email')) {
            $user->email = $request->email;
        }
        
        if ($request->exists('password') ) {
            $user->password = empty($request->password) ? $user->password : bcrypt($request->password);
        }

        if ($request->exists('blocked')) {
            $user->blocked = $request->blocked;
        }

        if ($request->has('privileges')) {
            $user->privileges()->sync($request->privileges);
        }

        $user->save();

        $response = response();

        if ($nextUrl) {
            $response = $response->redirectTo($nextUrl);
        } else {
            $response = $response->redirectToAction('Web\Admin\OrganizationController@view',
                ['id' => $user->organization_id]
            );
        }

        $response = $response->withSuccess('Usuario actualizado exitosamente.');

        return $response;
    }

    public function destroy(User $user)
    {
        $user->delete();
        $message = "Usuario eliminado exitosamente.";
        return back()->withSuccess($message);
    }
}
