<?php

namespace IrisGPS\Http\Controllers\Web;

use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Http\Requests;
use Illuminate\Http\Request;
use IrisGPS\Trackable;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth('web')->check()) {
            $user = auth('web')->user();
            if ($user->organization && $user->organization->locked) {
                return response()->redirectToAction('Web\DashboardController@lockedAccount');
            }
        }
        return view('web.dashboard');
    }

    public function lockedAccount(){
        return view('web.account.locked-organization');
    }
}