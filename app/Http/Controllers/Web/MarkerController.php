<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Marker;

use Maatwebsite\Excel\Facades\Excel;

class MarkerController extends Controller
{
    public function __construct()
    {
        if(auth('admin')->check()) {

        }
        else {
            $this->middleware('verify-organization');
            $this->middleware('verify-privilege:view-marker', ['only' => 'view']);
            $this->middleware('verify-privilege:create-marker', ['only' => 'create']);
            $this->middleware('verify-privilege:store-marker', ['only' => 'store']);
            $this->middleware('verify-privilege:edit-marker', ['only' => ['edit', 'update']]);
            $this->middleware('verify-privilege:destroy-marker', ['only' => 'destroy']);
        }
        
    }

    public function index(Request $request, $format = 'html')
    {

        $searchText = $request->search_text ? $request->search_text : null;
        $searchType = $request->search_type ? $request->search_type : null;
        
        $searchTypes = array(
            'name' => 'Nombre',
            'address' => 'Dirección',
            'phone' => 'Teléfono',
            'reference' => 'Referencia'
        );

        $organization_id = (auth('web')->check() ? auth('web')->user()->organization_id : null);

        //Building query
        $markers = Marker::query();

        if ($searchType && $searchText) {
                $markers = $markers->where(
                    $searchType,
                    'LIKE',
                    '%' . $searchText . '%'
                );
        }

        $markers->where('markers.organization_id', $organization_id);

        //Building paginator
        $markers = $markers->paginate(20);

        //Building response
        switch ($format) {
            case 'xls':
                Excel::create('Marcadores', function($excel) use ($markers) {
                    $excel->sheet('Marcadores', function($sheet) use ($markers) {
                        $sheet->row(1, ['id',
                            'Nombre',
                            'Dirección',
                            'Teléfono',
                            'Referencia',
                            'lat',
                            'lng',
                            'Registrado',
                            'Actualizado'
                            ]);

                        $i = 2;

                        foreach ($markers as $value) {
                            $sheet->row($i, [
                                $value->id,
                                $value->name,
                                $value->address,
                                $value->phone,
                                $value->reference,
                                $value->lat,
                                $value->lng,
                                $value->created_at,
                                $value->updated_at
                                ]);
                            $i++;
                        }
                    });

                })->download();

                break;
            case 'html':

                $hasPrivilegesToCreate = auth()->user()->hasPrivilege('create-marker');
                $hasPrivilegesToView = auth()->user()->hasPrivilege('view-marker');
                $hasPrivilegesToEdit = auth()->user()->hasPrivilege('edit-marker');
                $hasPrivilegesToDestroy = auth()->user()->hasPrivilege('destroy-marker');

                return view('web.tasks.markers.index', compact(
                        'markers',
                        'searchText',
                        'searchType',
                        'searchTypes',
                        'hasPrivilegesToCreate',
                        'hasPrivilegesToView',
                        'hasPrivilegesToEdit',
                        'hasPrivilegesToDestroy'
                    ));
                break;
        }
        
    }

    public function create()
    {
        return view('web.tasks.markers.create');

    }

    public function store()
    {

    }

    public function view(Marker $marker)
    {
        return view('web.tasks.markers.view')->with(compact('marker'));
    }

    public function edit(Marker $marker)
    {
        return view('web.tasks.markers.edit')->with(compact('marker'));
    }

    public function update()
    {

    }

    public function destroy(Marker $marker)
    {
        $this->validateMarkerProperty($marker);
        $marker->delete();
        return back()->withSuccess('Destino eliminado exitosamente');
    }

    protected function validateMarkerProperty($model)
    {
        if ($model->organization_id !== auth()->user()->organization_id) {
            abort(404);
        }
    }
}
