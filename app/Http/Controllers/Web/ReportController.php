<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use Carbon\Carbon;

class ReportController extends Controller
{
    public function __construct()
    {
        if(!auth('admin')->check()) {
            $this->middleware('verify-organization');
            $this->middleware('verify-privilege:manage-reports', ['only' => ['index', 'a', 'b', 'c']]);
        }
    }

    public function index()
    {
    	return view('web.reports.index');
    }

    public function a()
    {
    	return view('web.reports.report-a');
    }

    public function b()
    {
    	return view('web.reports.report-b');
    }

    public function c()
    {
    	return view('web.reports.report-c');
    }

    public function totalDistanceTraveledOfAllAgentsOfOrganization()
    {
        $startDate = Carbon::now()->startOfMonth()->toDateString();
        $endDate = Carbon::now()->endOfMonth()->toDateString();

        $data = [
            'startDate' => $startDate,
            'endDate' => $endDate
        ];

        return view('web.reports.total-distance-traveled-of-all-agents', $data);
    }
}
