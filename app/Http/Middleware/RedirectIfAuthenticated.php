<?php

namespace IrisGPS\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if ($guard == 'web' || $guard == null) {
                return redirect('dashboard');
            } elseif ($guard == 'admin' ) {
                return redirect('admin/dashboard');
            }
        }

        return $next($request);
    }
}
