<?php

namespace IrisGPS\Http\Requests;

use IrisGPS\User;
use IrisGPS\Http\Requests\Request;

class UpdateOrDeleteUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $userModel = $this->route('userModel');
        return User::where('id', $userModel->id)
            ->where('organization_id', auth()->user()->organization_id)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|min:4',
            'email' => 'required|max:255|email|unique:users,email,'.$this->email.',email',
            'password' => 'alpha_num|max:255|min:6',
        ];
    }

    public function forbiddenResponse()
    {
        return response()->redirectToAction('WebDashboardController@dashboard');
    }
}
