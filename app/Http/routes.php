<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

app('router')->group(['domain' => '{subdomain}.test.dev'], function ($subdomain) {
    app('router')->get('/', function ($subdomain) {
        if ($subdomain != 'paul') {
            return redirect()->to('http://test.dev');
        }
        echo $subdomain;
    });
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

app('router')->group(['middleware' => ['web']], function () {

    app('router')->group(['prefix' => 'test'], function() {
        app('router')->get('/', function() {
            return dd(auth('web')->user()->organization->isCurrentPlanExpired());
        });
    });

    app('router')->get('/', 'Web\LandingController@landing');

    // Admin's routes
    app('router')->group(['prefix' => 'admin'], function () {

        app('router')->get('/', 'Web\Admin\HomeController@index');

        app('router')->get('/dashboard', 'Web\Admin\HomeController@index');

        multiAuthRoutes('Auth\AdminAuthController', 'Auth\PasswordController');

        app('router')->group(['middleware' => ['auth:admin']], function () {

            app('router')->get('/dashboard', 'Web\Admin\HomeController@index');

            app('router')->group(['prefix' => 'organizations'], function () {

                app('router')->get('/', 'Web\Admin\OrganizationController@index');
                app('router')->get('/{id}', 'Web\Admin\OrganizationController@view');
                app('router')->get('/{id}/edit', 'Web\Admin\OrganizationController@edit');
                app('router')->put('/{id}', 'Web\Admin\OrganizationController@update');
                app('router')->post('/{id}/destroy', 'Web\Admin\OrganizationController@destroy');
                app('router')->post('/{id}/lock', 'Web\Admin\OrganizationController@lock');
                app('router')->post('/{id}/unlock', 'Web\Admin\OrganizationController@unlock');

                app('router')->group(['prefix' => '{organization_id}/users'], function () {
                    app('router')->get('/', 'Web\Admin\OrganizationUserController@index');
                    app('router')->any('/view/{userModel}', 'Web\Admin\OrganizationUserController@view');
                    app('router')->get('/edit/{userModel}', 'Web\Admin\OrganizationUserController@edit');
                    app('router')->post('/edit/{userModel}', 'Web\Admin\OrganizationUserController@update');
                    app('router')->post('/destroy/{userModel}', 'Web\Admin\OrganizationUserController@destroy');
                    app('router')->post('lock/{userModel}', 'Web\Admin\OrganizationUserController@lock');
                    app('router')->post('unlock/{userModel}', 'Web\Admin\OrganizationUserController@unlock');
                    app('router')->get('/index.{format}', 'Web\Admin\OrganizationUserController@index');
                });

                app('router')->group(['prefix' => '{organization_id}/payments'], function () {
                    app('router')->get('/', 'Web\Admin\OrganizationPaymentController@index');
                    app('router')->any('/view/{paymentId}/image', 'Web\Admin\OrganizationPaymentController@viewImage');
                    app('router')->any('/view/{paymentId}', 'Web\Admin\OrganizationPaymentController@view');
                    app('router')->get('create', 'Web\Admin\OrganizationPaymentController@create');
                    app('router')->get('/edit/{paymentId}', 'Web\Admin\OrganizationPaymentController@edit');
                    app('router')->post('/edit/{paymentId}', 'Web\Admin\OrganizationPaymentController@update');
                    app('router')->post('/', 'Web\Admin\OrganizationPaymentController@store');
                    app('router')->post('/destroy/{paymentId}', 'Web\Admin\OrganizationPaymentController@destroy');
                    app('router')->get('/index.{format}', 'Web\Admin\OrganizationPaymentController@index');
                });

            });

            app('router')->group(['prefix' => 'users'], function() {
                app('router')->get('create', 'Web\Admin\UserController@create');
                app('router')->post('create', 'Web\Admin\UserController@store');
                app('router')->any('view/{userModel}', 'Web\Admin\UserController@view');
                app('router')->get('edit/{userModel}', 'Web\Admin\UserController@edit');
                app('router')->post('edit/{userModel}', 'Web\Admin\UserController@update');
                app('router')->post('destroy/{userModel}', 'Web\Admin\UserController@destroy');
                app('router')->get('/index.{format}', 'Web\Admin\UserController@index');
                app('router')->get('/', 'Web\Admin\UserController@index');
            });

            app('router')->group(['prefix' => 'suggestions'], function () {
                app('router')->get('', 'Web\Admin\SuggestionController@index');
                app('router')->post('/destroy/{suggestionId}', 'Web\Admin\SuggestionController@destroy');
            });

            app('router')->group(['prefix' => 'reports'], function () {
                app('router')->get('/', 'Web\Admin\ReportController@index');
                app('router')->get('/a', 'Web\Admin\ReportController@a');
                app('router')->get('/b', 'Web\Admin\ReportController@b');
                app('router')->get('/c', 'Web\Admin\ReportController@c');
            });
        });

        
    });
    // End Admin's routes


    // Web organization
    app('router')->get('user/activation/{token}', 'Auth\WebAuthController@activateUser')->name('user.activate');


    multiAuthRoutes('Auth\WebAuthController', 'Auth\PasswordController');

    app('router')->group(['middleware' => 'auth:web'], function () {

        app('router')->get('/dashboard', 'Web\DashboardController@index');

        app('router')->get('/suggestions/create', 'Web\SuggestionController@create');

        app('router')->get('/locked-account', 'Web\DashboardController@lockedAccount');

        app('router')->group(['prefix' => 'trackable-assets'], function() {
            app('router')->get('/index.{format}', 'Web\TrackableController@index');
            app('router')->get('/', 'Web\TrackableController@index');

            app('router')->post('destroy/{trackableModel}', 'Web\TrackableController@destroy');

            app('router')->group(['prefix' => 'agent'], function() {
                app('router')->get('create', 'Web\TrackableController@createAgent');
                app('router')->post('create', 'Web\TrackableController@storeAgent');
                app('router')->any('view/{agentModel}', 'Web\TrackableController@viewAgent');
                app('router')->get('edit/{agentModel}', 'Web\TrackableController@editAgent');
                app('router')->post('edit/{agentModel}', 'Web\TrackableController@updateAgent');
                app('router')->get('geolocation-history/{agentId}', 'Web\TrackableController@geolocationHistoryAgent');
            });

            app('router')->group(['prefix' => 'vehicle'], function() {
                app('router')->get('create', 'Web\TrackableController@createVehicle');
                app('router')->post('create', 'Web\TrackableController@storeVehicle');
                app('router')->any('view/{vehicleModel}', 'Web\TrackableController@viewVehicle');
                app('router')->get('edit/{vehicleModel}', 'Web\TrackableController@editVehicle');
                app('router')->post('edit/{vehicleModel}', 'Web\TrackableController@updateVehicle');
            });
        });

        app('router')->group(['prefix' => 'tasks'], function() {
            app('router')->get('create', 'Web\TaskController@create');
            app('router')->post('create', 'Web\TaskController@store');
            app('router')->any('view/{taskModel}', 'Web\TaskController@view');
            app('router')->get('edit/{taskModel}', 'Web\TaskController@edit');
            app('router')->post('edit/{taskModel}', 'Web\TaskController@update');
            app('router')->post('destroy/{taskModel}', 'Web\TaskController@destroy');
            app('router')->get('/index.{format}', 'Web\TaskController@index');
            app('router')->get('/', 'Web\TaskController@index');
            app('router')->get('/calendar', 'Web\TaskController@calendar');
        });

        app('router')->group(['prefix' => 'markers'], function() {
            app('router')->get('create', 'Web\MarkerController@create');
            app('router')->post('create', 'Web\MarkerController@store');
            app('router')->any('view/{markerModel}', 'Web\MarkerController@view');
            app('router')->get('edit/{markerModel}', 'Web\MarkerController@edit');
            app('router')->post('edit/{markerModel}', 'Web\MarkerController@update');
            app('router')->post('destroy/{markerModel}', 'Web\MarkerController@destroy');
            app('router')->get('/index.{format}', 'Web\MarkerController@index');
            app('router')->get('/', 'Web\MarkerController@index');
        });

        app('router')->group(['prefix' => 'users'], function() {
            app('router')->get('create', 'Web\UserController@create');
            app('router')->post('create', 'Web\UserController@store');
            app('router')->any('view/{userModel}', 'Web\UserController@view');
            app('router')->get('edit/{userModel}', 'Web\UserController@edit');
            app('router')->post('edit/{userModel}', 'Web\UserController@update');
            app('router')->post('destroy/{userModel}', 'Web\UserController@destroy');
            app('router')->get('/index.{format}', 'Web\UserController@index');
            app('router')->get('/', 'Web\UserController@index');
        });

        app('router')->group(['prefix' => 'account'], function() {
            app('router')->get('/edit', 'Web\AccountController@edit');
            app('router')->post('/edit', 'Web\AccountController@update');
            app('router')->get('/', 'Web\AccountController@index');
        });

        app('router')->group(['prefix' => 'reports'], function () {
            app('router')->get('/total-distance-traveled-of-agents', 'Web\ReportController@totalDistanceTraveledOfAllAgentsOfOrganization');      
        });

    });

    app('router')->group(['prefix' => 'reports'], function () {
        app('router')->get('/', 'Web\ReportController@index');
        app('router')->get('/a', 'Web\ReportController@a');
        app('router')->get('/b', 'Web\ReportController@b');
        app('router')->get('/c', 'Web\ReportController@c');
    });
    // End web Organization
});


// Api
app('router')->group(['prefix' => 'api'], function () {

    app('router')->group(['namespace' => 'Auth\Api', 'middleware' => 'api'], function () {

        app('router')->group(['prefix' => 'auth'], function () {
            app('router')->post('login', 'TrackableAuthController@login');
            app('router')->any('logout', 'TrackableAuthController@logout');

            app('router')->group(['prefix' => 'me'], function () {
                app('router')->post('update-notification-id', 'TrackableAuthController@updateNotificationId');
                app('router')->post('store-geolocation', 'TrackableAuthController@storeGeolocation');
                app('router')->any('tasks', 'TrackableAuthController@taskList');
                app('router')->any('check-list-item', 'TrackableAuthController@checkListItem');
                app('router')->any('uncheck-list-item', 'TrackableAuthController@uncheckListItem');
                app('router')->any('reschedule-visit-point', 'TrackableAuthController@rescheduleVisitPoint');
                app('router')->any('/', 'TrackableAuthController@me');
            });
        });

    });


    app('router')->group(['namespace' => 'Api'], function () {

        app('router')->group(['prefix' => 'organization', 'middleware' => 'api'], function () {
            app('router')->post('validate', 'OrganizationController@validateExistence');
        });

        // Markers
        app('router')->group(['prefix' => 'markers'], function () {
            app('router')->post('store', 'MarkerController@store');
            app('router')->post('update', 'MarkerController@update');
            app('router')->any('/{id?}', 'MarkerController@index');
        });

        // Agents
        app('router')->group(['prefix' => 'agents'], function () {
            app('router')->any('/', 'AgentController@index');
        });

        // Vehicles
        app('router')->group(['prefix' => 'vehicles'], function () {
            app('router')->any('/', 'VehicleController@index');
        });

        // Tasks
        app('router')->group(['prefix' => 'tasks'], function () {
            app('router')->post('store', 'TaskController@store');
            app('router')->post('update', 'TaskController@update');
            app('router')->post('saveVisitPoint', 'TaskController@saveVisitPoint');
            app('router')->post('/{id}', 'TaskController@index');
        });

        // Trackables
        app('router')->group(['prefix' => 'trackables'], function () {
            app('router')->post('disconnect', 'TrackableController@disconnect');
            app('router')->any('geolocation', 'TrackableController@getWithGeolocation');
        });

        //Suggestions
        app('router')->post('suggestions', 'SuggestionController@store');
    });

    app('router')->group(['prefix' => 'auth'], function () {

        //organization users
        app('router')->group(['prefix' => 'organization', 'middleware' => ['web', 'auth:web']], function () {
            app('router')->group(['prefix' => 'me'], function () {

                app('router')->group(['prefix' => 'trackables'], function () {
                    app('router')->group(['prefix' => '{trackableType}/{trackableId}'], function () {
                        app('router')->get(
                            'visit-points-report',
                            'Api\Organization\Me\TrackableController@visitPointsReport'
                        );
                    });
                });

                app('router')->group(['prefix' => 'visit-points'], function () {
                    app('router')->delete('/{id}', 'Api\Organization\Me\VisitPointController@destroy');
                    app('router')->get('/{visitPointId}/history', 'Api\AuthenticatedUserOrganizationVisitPoint@history');
                    app('router')->get('/rescheduled', 'Api\AuthenticatedUserOrganizationVisitPoint@rescheduled');
                    app('router')->get('/items-grouped-by-day', 'Api\Organization\Me\VisitPointController@itemsGroupedByDay');
                });

                app('router')->group(['prefix' => 'agents'], function () {
                    app('router')->group(['prefix' => '{agentId}'], function () {
                        app('router')->get('visit-points-by-day', 'Api\Organization\Me\AgentController@viewVisitPointsByDay');
                        app('router')->get('geolocation-history', 'Api\Organization\Me\AgentController@geolocationHistory');
                    });
                });

                app('router')->group(['prefix' => 'tasks'], function () {
                    app('router')->get('/', 'Api\Organization\Me\TaskController@index');
                    app('router')->get('/calendar', 'Api\Organization\Me\TaskController@calendar');
                });

                //Reports
                app('router')->group(['prefix' => 'reports'], function () {
                	app('router')->get('/total-distance-traveled-of-all-agents', 'Api\Organization\Me\ReportController@totalDistanceTraveledOfAllAgents');
                });
            });
        });

        // Admin users
        app('router')->group(['prefix' => 'admin', 'middleware' => ['web', 'auth:admin']], function () {
            app('router')->group(['prefix' => 'me'], function () {

                app('router')->group(['prefix' => 'organizations'], function () {

                    app('router')->get(
                        'items-grouped-by-day',
                        'Api\Admin\Me\OrganizationController@reportOfaccountsCreated'
                    );

                    app('router')->get(
                        'total-of-agents',
                        'Api\Admin\Me\OrganizationController@reportOfAgents'
                    );

                    app('router')->get(
                        'visits/all',
                        'Api\Admin\Me\OrganizationController@reportOfVisits'
                    );

                    app('router')->get(
                        '{organizationId}/visits/all',
                        'Api\Admin\Me\OrganizationController@reportOfVisits'
                    );

                    app('router')->get(
                        '',
                        'Api\Admin\Me\OrganizationController@index'
                    );
                });
            });
        });
        
        app('router')->group(['prefix' => 'api/admin', 'middleware' => ['web', 'auth:admin']], function () {
            app('router')->group(['prefix' => 'me'], function () {

                app('router')->group(['prefix' => 'organizations'], function () {

                    app('router')->get(
                        '',
                        'Api\Admin\Me\OrganizationController@index'
                    );
                });
            });
        }); 
        //Admin Users   
    });
});