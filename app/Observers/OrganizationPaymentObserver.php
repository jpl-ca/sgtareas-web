<?php
namespace IrisGPS\Observers;

use IrisGPS\Organization;

use Log;

class OrganizationPaymentObserver
{
	public function saved($model)
	{
		if (isset($model->organization_id)) {
			$organization = Organization::find($model->organization_id);

			$organization->subscription_ends = $model->subscription_end_date;

			$organization->save();

			Log::info("Organization changed its data");
		}
		
	}
}
