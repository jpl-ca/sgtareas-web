<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
