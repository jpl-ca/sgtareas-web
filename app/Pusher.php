<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class Pusher
{
	static $endpoint = "https://fcm.googleapis.com/fcm/";
	protected $receiver = null;
	protected $data = [];
	protected $appKey = null;

	function __constructor($attributes = array())
	{
		if (isset($attributes['receiver'])) {
			$this->receiver = $attributes['receiver'];	
		}

		if (isset($attributes['data'])) {
			$this->data = $attributes['data'];	
		}
	}

	public function addReceiver($receiver)
	{
		$this->receiver = $receiver;
	}

    public function addData($data)
    {
    	$this->data = $data;
    }

    public function send($options = [])
    {
    	if (isset($options['receiver'])) {
			$this->addReceiver($options['receiver']);
		}

		if (isset($options['data'])) {
			$this->addData($options['data']);
		}

    	$client = new \GuzzleHttp\Client(['base_uri' => self::$endpoint]);

    	$headers = [
    		'Content-Type' => 'application/json',
    		'Authorization' => 'key=' . config('app.google_app_key')
    	];

    	$body = $this->getJsonStringToSend();

    	$res = $client->request('POST', 'send', [
    		'headers' => $headers,
    		'body' => $body
		]);
    	return $res;
    }

    private function getJsonStringToSend()
    {
    	$json = [
    		'to' => $this->receiver,
    		'data' => $this->data
    	];

    	$jsonString = json_encode($json);

    	return $jsonString;
    }

}
