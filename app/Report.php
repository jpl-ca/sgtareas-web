<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use DB;

use Exception;

use Log;

class Report extends Model
{
    public static function totalSalesMadeByEachAgent($options = [])
    {
        $organizationId = "";

        $query = "
            SELECT
                agents.id   as agent_id,
                agents.first_name,
                count(product_orders.id) total_orders,
                IFNULL(sum(product_order_items.price), 0) total_sales,
                'PEN' as currency
            FROM 
                agents
            LEFT JOIN
                product_orders
                ON(
                    agents.id = product_orders.trackable_id
                    AND product_orders.created_at >= :startDate
                    AND product_orders.created_at <= :endDate
                )
            LEFT JOIN
                product_order_items
                ON(product_order_items.product_order_id = product_orders.id)
            WHERE
                agents.organization_id = :organizationId
            GROUP BY
                agents.id
        ";

        if (isset($options['organization_id'])) {
            $organizationId = $options['organization_id'];
        }   

        $startDate = Carbon::now()->addMonths(6);
        $endDate = Carbon::now();

        if (isset($options['startDate'])) {
            $startDate = Carbon::parse($options['startDate']);
        }

        if (isset($options['endDate'])) {
            $endDate = Carbon::parse($options['endDate']);
        }

        $rows = DB::select($query, [
            'startDate' => $startDate->toDateTimeString(),
            'endDate' => $endDate->toDateTimeString(),
            'organizationId' => $organizationId
        ]);

        return $rows;
    }

    public static function totalDistanceTraveledOfTrackable ($options = [])
    {
        if (!isset($options['trackableId'])) {
            throw new Exception("trackableId is not present");
        }

        $trackableId = $options['trackableId'];
        $totalDistance = (double) 0.000000000;
        
        $items = GeolocationHistory::where('trackable_id', $trackableId);

        if (isset($options['startDate'])) {
            $startDate = Carbon::parse($options['startDate'])->toDateTimeString();
            $items = $items->where('created_at', '>=', $startDate);
        }

        if (isset($options['endDate'])) {
            if (strlen($options['endDate']) == 10) {
                $endDate = Carbon::parse($options['endDate'])->addDays(1)->addSeconds(-1)->toDateTimeString();
            } else {
                $endDate = Carbon::parse($options['endDate'])->toDateTimeString();
            }
            
            $items = $items->where('created_at', '<=', $endDate);
        }

        $rows = $items->where('trackable_id', $trackableId)->get()->toArray();
        $sections = [];
        $indexOfSection = 0;
        $output = "";
        $i = 0;

        while($i < count($rows) - 1) {
            $pointA = $rows[$i];
            $pointB = $rows[$i+1];
            $dateTimeOfPointA = Carbon::parse($pointA['created_at']);
            $dateTimeOfPointB = Carbon::parse($pointB['created_at']);
            $diffInSeconds = abs($dateTimeOfPointA->diffInSeconds($dateTimeOfPointB));

            if ($diffInSeconds > 60) {
                $sections[$indexOfSection][] = $pointA;
                $indexOfSection++;
            } else {
                $sections[$indexOfSection][] = $pointA;
            }

            $i++;
        }

        foreach ($sections as $sectionKey => $section) {
            $items = $section;
            if (count($items) >= 2) {
                for ($i = 0;$i < count($items) - 1; $i++) {
                    $startPoint = $items[$i];
                    $endPoint = $items[$i+1];
                    $distance = distanceBetweenPoints(floatval($startPoint['lat']), floatval($startPoint['lng']), floatval($endPoint['lat']), floatval($endPoint['lng']), 'K');

                    $distance = is_nan($distance) ? 0 : $distance;
                    $totalDistance += $distance;
                }
            }
        }

        return $totalDistance;
    }

    public static function totalDistanceTraveledOfAllAgentsOfOrganization($options)
    {
        if (!isset($options['organizationId'])) {
            throw new Exception("organizationId option is required", 1);
        }

        $startDate = isset($options['startDate']) ? $options['startDate'] : null;
        $endDate = isset($options['endDate']) ? $options['endDate'] : null;

        $organization = Organization::find($options['organizationId']);

        $items = [];

        foreach ($organization->agents()->get() as $agent) {
            $trackableId = $agent->trackable->id;
            $distanceTraveled = $agent->totalDistanceTraveled([
                'startDate' => $startDate,
                'endDate' => $endDate
            ]);

            $items[] = [
                'agentId' => $agent->id,
                'agentFullName' => $agent->full_name,
                'distance' => $distanceTraveled
            ];
        }

        return $items;
    }
}
