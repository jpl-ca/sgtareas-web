<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
	protected $fillable = [
		'content'
	];

	public function organization()
	{
		return $this->belongsTo(Organization::class, 'organization_id', 'id');
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
}
