<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name')->unique();
            $table->string('ruc', 11)->unique()->nullable();
            $table->string('subscription_id');
            $table->dateTime('subscription_ends');
            $table->string('billing_name')->nullable();
            $table->string('billing_address')->nullable();
            $table->integer('agents_location_frequency')->default(10);
            $table->integer('vehicles_location_frequency')->default(10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organizations');
    }
}
