<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountryFieldsToOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->string("country")->nullable();
            $table->string("country_suffix")->nullable();
            $table->string("document_type")->nullable();
            $table->string("document_number")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organizations', function (Blueprint $table) {
            $table->dropColumn("country");
            $table->dropColumn("country_suffix");
            $table->dropColumn("document_type");
            $table->dropColumn("document_number");
        });
    }
}
