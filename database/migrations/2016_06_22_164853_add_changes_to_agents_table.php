<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChangesToAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::table('agents', function (Blueprint $table) {
            $conn = Schema::getConnection();
            $dbSchemaManager = $conn->getDoctrineSchemaManager();
            $doctrineTable = $dbSchemaManager->listTableDetails('agents');

            // alter table "users" add constraint users_email_unique unique ("email")
            if ($doctrineTable->hasIndex('agents_authentication_code_unique')) {
                $table->dropUnique('agents_authentication_code_unique');
            }

            if ($doctrineTable->hasIndex('agents_authentication_code_organization_id_unique')) {
                $table->dropUnique('agents_authentication_code_organization_id_unique');
            }

            $table->unique(['authentication_code', 'organization_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
