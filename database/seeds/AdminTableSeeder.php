<?php

use Illuminate\Database\Seeder;

use IrisGPS\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
        	'name' => 'Admin',
        	'email' => 'admin@yopmail.com',
        	'password' => bcrypt('password')
        ]);
    }
}
