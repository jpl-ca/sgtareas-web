<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypeTableSeeder::class);
        $this->call(OrganizationTableSeeder::class);
        $this->call(PrivilegeTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(VisitStateTableSeeder::class);
        $this->call(TrackableTableSeeder::class);
        $this->call(MarkerTableSeeder::class);
        $this->call(TaskTableSeeder::class);
        $this->call(AdminTableSeeder::class);
    }
}
