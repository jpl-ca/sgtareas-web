<?php

use Illuminate\Database\Model;
use Illuminate\Database\Seeder;
use IrisGPS\Marker;
use Illuminate\Support\Facades\DB;

class MarkerTableSeeder extends Seeder
{
	/**
	* Run the database seeds
	*
	*/
	public function run()
	{
		$lat = -12.083202271933000;
		$lng = -76.990699775051000;
		$scale = 0.00001;
		for($i = 0; $i < 100;$i++){
			$rand_lat = rand(1, $scale);
			$rand_lng = rand(1, $scale);
			$new_marker = [
			"name" => "Marker ".str_pad($i,5, STR_PAD_LEFT),
			"address" => "Address ".str_pad($i,7, STR_PAD_LEFT),
			"phone" => str_pad(rand(1,9999),9, STR_PAD_RIGHT),
			"lat" => $lat + $rand_lat,
			"lng" => $lng + $rand_lng,
			"organization_id" => "pepe-sac"
			];
			DB::table('markers')->insert($new_marker);
		}
		
	}
}