<?php

use Illuminate\Database\Model;
use Illuminate\Database\Seeder;
use IrisGPS\Marker;
use Illuminate\Support\Facades\DB;
use IrisGPS\Task;
use IrisGPS\Trackable;

use Carbon\Carbon;

class TaskTableSeeder extends Seeder
{
	/**
	* Run the database seeds
	*
	*/
	public function run()
	{
		$date = Carbon::create()->toDateTimeString();
		
		for($i = 1; $i <= 10; $i++){

			$any_trackable = Trackable::all()->random(1);

			$new_task = [
				"start_date" => Carbon::now(),
				"end_date" => Carbon::now(),
				"description" => "description of task",
				"taskable_id" => $any_trackable->id,
				"taskable_type" => $any_trackable->trackable_type,
				"organization_id" => "pepe-sac",
				"created_at" => $date,
				"updated_at" => $date
			];

			DB::table('tasks')->insert($new_task);

			$markers = [];
			$markers =  Marker::all()->random(3);
			//array_push($markers, $marker);

			foreach($markers as $marker){

				$newTaskVisitPoint = [
					"lat" => $marker->lat,
					"lng" => $marker->lng,
					"name" => $marker->name,
					"address" => $marker->address,
					"phone" => $marker->phone,
					"reference" => $marker->reference,
					"marker_id" => $marker->id,
					"task_id" => $i,
					"datetime" => Carbon::now()->addMinutes(rand(0, 400)),
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now()
				];

				$newTaskVisitPoint_id = DB::table('tasks_visit_points')->insertGetId($newTaskVisitPoint, 'id');

				#Adding Items to checklist of visit point
				for($ii = 1; $ii <= 10; $ii++){
					$new_item = [
						"description" => "item ".str_pad($ii, 5),
						"checked" => 0,
						"tasks_visit_point_id" => $newTaskVisitPoint_id
						];

					DB::table('checklist_items')->insert($new_item);
				}
			}
		}

		
	}
}