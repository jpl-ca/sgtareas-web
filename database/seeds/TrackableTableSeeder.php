<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use IrisGPS\Trackable;
use IrisGPS\Agent;
use IrisGPS\Vehicle;

class TrackableTableSeeder extends Seeder
{
	/**
	* Run the database seeds
	*
	*/
	public function run()
	{
		Model::unguard();
		$date = Carbon\Carbon::create()->toDateTimeString();

		DB::table('agents')->insert([
			'id' => '1',
			'first_name' => 'Agente A0001',
			'last_name' => 'Perez',
			'authentication_code' => '123456789',
			'password' => bcrypt('password'),
			'organization_id' => 'pepe-sac',
			'created_at' => $date,
			'updated_at' => $date
			]);

		DB::table('trackables')->insert([
			'trackable_id' => '1',
			'trackable_type' => 'IrisGPS\Agent',
			'api_token' => 'GPPstCMemCbE1PPEnebmQGlAFFVweY2Y2hm0u6FwCGSR9N416qqr4EeEa9hf',
			'created_at' => $date,
			'updated_at' => $date
			]);

		DB::table('vehicles')->insert([
			'id' => '2',
			'plate' => 'ABC-123',
			'brand' => 'Marca001',
			'model' => 'Model',
			'color' => 'red',
			'manufacture_year' => '2010',
			'gas_consumption_rate' => '12',
			'password' => bcrypt('password'),
			'organization_id' => 'pepe-sac',
			'created_at' => $date,
			'updated_at' => $date
			]);

		DB::table('trackables')->insert([
			'trackable_id' => '2',
			'trackable_type' => 'IrisGPS\Vehicle',
			'api_token' => 'GPPstCMemCbE1PPEnebmQGlAFFVweY2Y2hm0u6FwCGSR9N416qqr4EeEa9he',
			'created_at' => $date,
			'updated_at' => $date
			]);
		Model::reguard();
	}
}