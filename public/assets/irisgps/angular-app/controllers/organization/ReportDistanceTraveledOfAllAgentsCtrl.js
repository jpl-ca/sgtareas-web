'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', []);

app.controller('ReportDistanceTraveledOfAllAgents', ['$scope', '$http', 'reportsService', '$timeout', '$window', function($scope, $http, reportsService, $timeout, $window)
{
    $scope.getItems = function() {

        var startDate = document.getElementById('start_date').value;
        var endDate = document.getElementById('end_date').value;

        reportsService.getDistanceTraveledOfAllAgents(startDate, endDate).then(function (data) {
            $scope.items = data.data;
        });
    }

    $scope.export = function () {
        var startDate = document.getElementById('start_date').value;
        var endDate = document.getElementById('end_date').value;
        var url = baseUrl + '/api/auth/organization/me/reports/total-distance-traveled-of-all-agents?startDate=' + startDate + '&endDate=' + endDate + '&format=xls';
        $window.open(url);
    }
}]);

$(function () {
    $('#start_date').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#end_date').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $("#start_date").on("dp.change", function (e) {
        $('#end_date').data("DateTimePicker").minDate(e.date);
    });
    $("#end_date").on("dp.change", function (e) {
        $('#start_date').data("DateTimePicker").maxDate(e.date);
    });
});