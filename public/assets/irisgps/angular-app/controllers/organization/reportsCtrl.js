'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', []);



app.controller('ReportsController', ['$scope', '$http', 'reportsService', '$timeout', function($scope, $http, reportsService, $timeout)
{
        var html = function(id) { return document.getElementById(id); };
        $scope.getAgents = function() {
             reportsService.getAgents()
                .then(function(agents){ 
                  $scope.agents = [];
                    $scope.agents = agents;
                    console.log($scope.agents);
                    var all = {
                      id: 'all',
                      first_name: 'Todos'
                    };
                    $scope.agents.push(all);
                    
                    $scope.selected_agent = agents[agents.length-1];
                }); 
        }
        $scope.getAgents();
        $scope.$watch('selected_agent', function(agent){
            if(agent){
                $scope.id = agent.id; 
                console.log(document.getElementById("selected_agent").value);  
                console.log($scope.id);  
            }               
        });
        console.log($scope.id);  
        $scope.getAllforAgent = function() {
            console.log(html("start_date").value + ' ' + html("end_date").value+ ' ' + html("selected_agent").value)
            if ($scope.id == undefined || $scope.id == "all") {
              $scope.chart = "";
              reportsService.getAll(html("start_date").value, html("end_date").value)
                .then(function(visits){ 
                    $scope.visits = visits;
                    if ($scope.visits.length > 0) {
                        for (var i = 0; i < $scope.visits.length; i++) {
                            delete $scope.visits[i]["cancelled"];
                        };
                    };
              console.log($scope.visits);
        $scope.chart = AmCharts.makeChart("chartdiv", {
                                    "type": "serial",
                                    "theme": "light",
                                    "legend": {
                                        "useGraphSettings": true
                                    }, 
                                    "dataProvider": $scope.visits,
                                    "categoryField": "date",
                                    "synchronizeGrid":true,
                                    "valueAxes": [{
                                        "id":"v1",
                                        "axisColor": "#009486",
                                        "axisThickness": 2,
                                        "axisAlpha": 1,
                                        "position": "left"
                                    }],
                                    "chartScrollbarSettings": {
                                        "graph": "g1",
                                        "usePeriod": "10mm",
                                        "position": "top"
                                    },
                                    "graphs": [{
                                        "valueAxis": "v1",
                                        "lineColor": "#8E24AA",
                                        "bullet": "round",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Programado",
                                        "valueField":"scheduled",
                                        "fillAlphas": 0
                                    }, {
                                        "valueAxis": "v1",
                                        "lineColor": "#00BCD4",
                                        "bullet": "square",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Reprogramdo",
                                        "valueField":"rescheduled",
                                        "fillAlphas": 0
                                    }, {
                                        "valueAxis": "v1",
                                        "lineColor": "#3F51B5",
                                        "bullet": "triangleUp",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Hecho",
                                        "valueField":"done",
                                        "fillAlphas": 0
                                    }
                                    // , {
                                    //     "valueAxis": "v1",
                                    //     "lineColor": "blue",
                                    //     "bullet": "diamond",
                                    //     "bulletBorderThickness": 1,
                                    //     "hideBulletsCount": 30,
                                    //     "title":"total",
                                    //     "valueField":"total",
                                    //     "fillAlphas": 0
                                    // }
                                    ],
                                      "chartCursor": {
                                          "pan": true,
                                          "valueLineEnabled": true,
                                          "valueLineBalloonEnabled": true,
                                          "cursorAlpha":1,
                                          "cursorColor":"#258cbb",
                                          "limitToGraph":"g1",
                                          "valueLineAlpha":0.2,
                                          "valueZoomable":true
                                      },
                                    "categoryAxis": {
                                        "parseDates": true,
                                        "axisColor": "#DADADA",
                                        "minorGridEnabled": true
                                    },
                                    "export": {
                                        "enabled": true,
                                        "position": "top-right", 
                                        "menu": [{
                                            "class": "export-main",
                                            "menu": [{
                                            "format": "XLSX",
                                            "label": "Exportar Excel",
                                            "title": "Exporta a Excel",
                                          },{
                                            "format": "PDF",
                                            "label": "Exportar PDF",
                                            "title": "Exporta a PDF",
                                          },{
                                            "format": "CSV",
                                            "label": "Exportar CSV",
                                            "title": "Exporta a CSV",
                                          },]
                                        }]
                                    } 
                  
                                });
           $scope.zoomChart = function(){
                                    $scope.chart.zoomToIndexes($scope.chart.dataProvider.length - 20, $scope.chart.dataProvider.length - 1);
                                }
                                $scope.chart.addListener("dataUpdated",   $scope.zoomChart);
                                $scope.zoomChart; 
            }); 
            }else if($scope.id != "all" || $scope.id != undefined){
              reportsService.getAllforAgent($scope.id, html("start_date").value, html("end_date").value)
                .then(function(visits){ 
                    $scope.visits = visits.data;
                    if ($scope.visits.length > 0) {
                        for (var i = 0; i < $scope.visits.length; i++) {
                            delete $scope.visits[i]["cancelled"];
                        };
                    };
              console.log($scope.visits);
                     $scope.chart = AmCharts.makeChart("chartdiv", {
                                    "type": "serial",
                                    "theme": "light",
                                    "legend": {
                                        "useGraphSettings": true
                                    }, 
                                    "dataProvider": $scope.visits,
                                    "categoryField": "date",
                                    "synchronizeGrid":true,
                                    "valueAxes": [{
                                        "id":"v1",
                                        "axisColor": "#009486",
                                        "axisThickness": 2,
                                        "axisAlpha": 1,
                                        "position": "left"
                                    }],
                                    "chartScrollbarSettings": {
                                        "graph": "g1",
                                        "usePeriod": "10mm",
                                        "position": "top"
                                    },
                                    "graphs": [{
                                        "valueAxis": "v1",
                                        "lineColor": "#8E24AA",
                                        "bullet": "round",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Programado",
                                        "valueField":"scheduled",
                                        "fillAlphas": 0
                                    }, {
                                        "valueAxis": "v1",
                                        "lineColor": "#00BCD4",
                                        "bullet": "square",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Reprogramdo",
                                        "valueField":"rescheduled",
                                        "fillAlphas": 0
                                    }, {
                                        "valueAxis": "v1",
                                        "lineColor": "#3F51B5",
                                        "bullet": "triangleUp",
                                        "bulletBorderThickness": 1,
                                        "hideBulletsCount": 30,
                                        "title":"Hecho",
                                        "valueField":"done",
                                        "fillAlphas": 0
                                    }
                                    // , {
                                    //     "valueAxis": "v1",
                                    //     "lineColor": "blue",
                                    //     "bullet": "diamond",
                                    //     "bulletBorderThickness": 1,
                                    //     "hideBulletsCount": 30,
                                    //     "title":"total",
                                    //     "valueField":"total",
                                    //     "fillAlphas": 0
                                    // }
                                    ],
                                      "chartCursor": {
                                          "pan": true,
                                          "valueLineEnabled": true,
                                          "valueLineBalloonEnabled": true,
                                          "cursorAlpha":1,
                                          "cursorColor":"#258cbb",
                                          "limitToGraph":"g1",
                                          "valueLineAlpha":0.2,
                                          "valueZoomable":true
                                      }, 
                                    "categoryAxis": {
                                        "parseDates": true,
                                        "axisColor": "#DADADA",
                                        "minorGridEnabled": true
                                    },
                                    "export": {
                                        "enabled": true,
                                        "position": "top-right", 
                                        "menu": [{
                                            "class": "export-main",
                                            "menu": [{
                                            "format": "XLSX",
                                            "label": "Exportar Excel",
                                            "title": "Exporta a Excel",
                                          },{
                                            "format": "PDF",
                                            "label": "Exportar PDF",
                                            "title": "Exporta a PDF",
                                          },{
                                            "format": "CSV",
                                            "label": "Exportar CSV",
                                            "title": "Exporta a CSV",
                                          },]
                                        }]
                                    } 
                  
                                });
                                                    
             
                $scope.zoomChart = function(){
                    $scope.chart.zoomToIndexes($scope.chart.dataProvider.length - 20, $scope.chart.dataProvider.length - 1);
                }
                $scope.chart.addListener("dataUpdated",   $scope.zoomChart);
                $scope.zoomChart; 

                }); 


            }
        }


}]);

    $(function () {
            $('#start_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#end_date').datetimepicker({
                    useCurrent: false,
                    format: 'YYYY-MM-DD'
            });
            $("#start_date").on("dp.change", function (e) {
                $('#end_date').data("DateTimePicker").minDate(e.date);
            });
            $("#end_date").on("dp.change", function (e) {
                $('#start_date').data("DateTimePicker").maxDate(e.date);
            });
    });

    