'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var irisGpsApp = angular.module('app', []);

irisGpsApp.factory('Service', function($http) {
    return {
        calendar: function () {
            return $http.get(base_url +'/api/auth/organization/me/tasks/calendar').then(function (response) {
                console.log(response);
                return response.data;
            });
        },
        markers: function () {
            return $http.get(base_url + '/api/markers').then(function (response) {
                return response.data;
            });
        }
        ,
        addruta: function(ruta) {
            return $http.post(baseUrl + '/api/tasks/store', ruta).then(function (response) {
                return response.data;                
            });
        }
        ,
        updateruta: function(ruta) {
            return $http.post(baseUrl + '/api/tasks/update', ruta).then(function (response) {
                return response.data;                
            });
        }
        ,
        deleteruta: function(key) {
            return $http.delete(baseUrl + '/api/auth/organization/me/visit-points/' + key).then(function (response) {
                return response.data;                
            });
        }
    };
});

irisGpsApp.controller('taskController', [
    '$scope',
    '$http',
    'Service',
    'AgentService',
    'TaskService',
    function(
            $scope,
            $http,
            Service,
            AgentService,
            TaskService
        )
    {
        // Initial variables
        var html = function(id) { return document.getElementById(id); };
        $scope.agents = [];
        $scope.checklist = [];
        $scope.rawDataOfCalendar = null;

        $scope.modSchedHeight = function (){
            var headHeight = 100;
            var sch = document.getElementById("scheduler_here");
            sch.style.height = (parseInt(document.body.offsetHeight)-headHeight)+"px";
        }

        $scope.loadMarkers = function() {
             Service.markers()
                .then(function(markers){  
                    $scope.markers = markers;
                }); 
        }        

        $scope.loadCalendar = function() {
            scheduler.locale.labels.timeline_tab = "Timeline";
            scheduler.locale.labels.section_custom = "Section";
            scheduler.locale.labels.section_important = "Important";
            scheduler.config.details_on_create=true;
            scheduler.config.details_on_dblclick=true;
            scheduler.config.xml_date="%Y-%m-%d";
            scheduler.config.prevent_cache = true;

            Service.calendar()
                 .then(function(calendar){
                        scheduler.clearAll();

                        $scope.trackables = calendar.data.trackables;
                        $scope.agentes = calendar.data.trackables;

                        scheduler.createTimelineView({
                            name:   "timeline",
                            x_unit: "day",
                            x_date: "%d %l",
                            x_step: 1,
                            x_size: 7,
                            x_start: 0,
                            x_length: 7,
                            y_unit: $scope.agentes,
                            y_property: "trackable_id",
                            render:"bar",
                            round_position: true
                        });

            scheduler.config.drag_move = false;
            scheduler.date.timeline_start = scheduler.date.week_start;
            scheduler.date.add_timeline = function(date, step, something) {
                return scheduler.date.add(date, step * 7, "day");
            };
            scheduler.init('scheduler_here', new Date(), "timeline");
            
            var block_id = null; 
            var startDate = new Date(); 
            scheduler.attachEvent("onBeforeViewChange", function(old_mode,old_date,mode,date){
                console.log("on beafore view change");

            if(block_id)
            scheduler.deleteMarkedTimespan(block_id);   
            var from = scheduler.date[mode + "_start"](new Date(date));
            var to = new Date(Math.min(new Date(startDate), + scheduler.date.add(from, 0, mode)));
            
            block_id = scheduler.addMarkedTimespan({
              start_date: from, 
              end_date:to,
              type:"dhx_time_block"
            });
              
                return true;
            });
            startDate.setDate(startDate.getDate()-1); 
            
            scheduler.config.limit_start = new Date(startDate);
            scheduler.config.limit_end = new Date(9999, 1,1);
            setInterval(function(){     
               scheduler.config.limit_start = new Date(startDate);
            }, 1000*60);

            $scope.vpoints = calendar.data.visit_points;  
            scheduler.parse($scope.vpoints, "json");

                    }); 
            
            

         scheduler.showLightbox = function (id) {            
            var ev = scheduler.getEvent(id);
            console.log("este el evento");
            console.log(ev);
            //scheduler.startLightbox(id, html("my_form2"));
             $scope.show_form_from_event(ev);
        };

        scheduler.attachEvent("onDblClick", function (id, e){
            var ev = scheduler.getEvent(id);    
            $scope.show_form_from_event(ev);
          });
        } //end load calendar

        $scope.addItemChecklist = function() {
            if (!$scope.itemlist) {
                return false;
            }

            if($scope.checklist == undefined){
                $scope.checklist = [];
                $scope.checklist.push({description:$scope.itemlist});
                $scope.itemlist = '';
            }else{
                $scope.checklist.push({description:$scope.itemlist});
                $scope.itemlist = '';
            }
             
        }
        $scope.removeItemChecklist = function(index) {
             $scope.checklist.splice(index,1);
        }

        $scope.destroy_task_visit_point = function (ev) {
            console.log("Destruyendo...");
            var url = "/api/auth/organization/me/visit-points/" + ev.key;
            
            $http({
                method: 'DELETE',
                url: url
            }).success(function (response) {
                console.log("Eliminado exitosamente");
            });
        }

        $scope.save_event = function (visitPoint) {
            console.log("********* SAVING VISIT POINT **************");
            console.log(visitPoint);
            TaskService.saveVisitPoint(visitPoint).success(function (response) {
                console.log("Servidor acaba de salvar visitPoint con response:");
                console.log(response);
                $scope.clearAndLoadTimeLine();
            }).error(function (response) {
                console.log(response);
            });
        }

        $scope.set_task_id_to_event_from_events = function(ev, events) {
            for (var i in events) {
                if (events[i].id && ev.id != events[i].id) {
                    ev.task_id = events[i].id;
                }    
            }
        }

        $scope.formReadyToRegister = function () {
            if ($scope.selectedVisitPoint.selectedMarker &&
                $scope.selectedVisitPoint.selectedTrackable &&
                ($scope.selectedVisitPoint.checklist && $scope.selectedVisitPoint.checklist.length > 0)
                ) {
                return true;
            }
            return false;
        }

        $scope.show_form_from_event = function (ev) {
            
            console.log("mostrando evento");
            console.log(ev);

            $scope.resetSelectedVisitPoint();

            $('.modal').on('show.bs.modal', function (e) {
                $scope.draw_event_into_event_form(ev);
                $scope.$apply();
            });
            $('.modal').modal({show: true});
        }

        $scope.draw_event_into_event_form = function (ev) {

            $scope.selectedVisitPoint = {
                checklist: []
            };

            $scope.selectedVisitPoint.time = ev.time;
            $scope.selectedVisitPoint.key = ev.key;

            if (ev.checklist) $scope.selectedVisitPoint.checklist = ev.checklist;

            $scope.selectedVisitPoint.event_id = ev.id;
            $scope.selectedVisitPoint.name = ev.name;
            $("#eventForm #time").val(ev.time);
            
            if (!ev.datetime) {
                $scope.selectedVisitPoint.datetime = moment(ev.start_date).format("YYYY-MM-DD h:mm:ss");
            } else {
                $scope.selectedVisitPoint.datetime = ev.datetime;
            }

            $scope.selectedVisitPoint.selectedTrackable = $scope.findTrackableById(ev.trackable_id);
            $scope.selectedVisitPoint.selectedMarker = $scope.findMarkerById(ev.marker_id);
        }

        $scope.findTrackableById = function (trackableId) {  
            for(var i in $scope.trackables) {
                if ($scope.trackables[i].key == trackableId) {
                    return $scope.trackables[i];
                    break;
                }
            }
        }

        $scope.findMarkerById = function (markerId) { 
            for(var i in $scope.markers) {
                if ($scope.markers[i].id == markerId) {
                    return $scope.markers[i];
                    break;
                }
            }
        }

        $scope.addItemToVisitPoint = function () {
            if (!$scope.selectedVisitPoint.newItem || $scope.selectedVisitPoint.newItem.length == 0) {
                return false;
            }

            var newItem = {
                description: $scope.selectedVisitPoint.newItem
            };

            $scope.selectedVisitPoint.checklist.push(newItem);

            $scope.selectedVisitPoint.newItem = "";
        }

        $scope.removeItemToSelectedVisitPoint = function (index) {

            if ( $scope.selectedVisitPoint.checklist && $scope.selectedVisitPoint.checklist.length > 0) {
                $scope.selectedVisitPoint.checklist.splice(index,1);
            }
        }

        $scope.saveSelectedVisitPoint = function () {
            var ev = scheduler.getEvent($scope.selectedVisitPoint.event_id);
            $scope.parseSelectedVisitPoint();
            //$scope.copySelectedVisitPointToEvent($scope.selectedVisitPoint, ev);
            $scope.save_event($scope.selectedVisitPoint);
            $scope.hideSelectedVisitPointModal();
        }

        $scope.parseSelectedVisitPoint = function () {

            if ($scope.selectedVisitPoint.selectedTrackable) {
                $scope.selectedVisitPoint.trackable_id = $scope.selectedVisitPoint.selectedTrackable.key;
            }

            if ($scope.selectedVisitPoint.selectedMarker) {

                $scope.selectedVisitPoint.marker_id = $scope.selectedVisitPoint.selectedMarker.id;
                $scope.selectedVisitPoint.lat = $scope.selectedVisitPoint.selectedMarker.lat;
                $scope.selectedVisitPoint.lng = $scope.selectedVisitPoint.selectedMarker.lng;
                $scope.selectedVisitPoint.name = $scope.selectedVisitPoint.selectedMarker.name;
            }

            $scope.selectedVisitPoint.time = $("#eventForm #time").val();
        }


        $scope.hideSelectedVisitPointModal = function () {
            $('.modal').modal('hide');
        }

        $scope.destroySelectedVisitPoint = function () {
            var ev = scheduler.getEvent($scope.selectedVisitPoint.event_id);

            if (ev.key) {
                $scope.destroy_task_visit_point(ev);
            }
            scheduler.deleteEvent(ev.id);
            $scope.hideSelectedVisitPointModal();
        }

        $scope.clearAndLoadTimeLine = function () {
            Service.calendar()
                .then( function (calendar) {
                    $scope.rawDataOfCalendar = calendar.data;
                    scheduler.clearAll();
                    scheduler.parse(calendar.data.visit_points, "json");
                    $scope.setTrackablesFromRawDataOfCalendar();
                });
        }

        $scope.resetSelectedVisitPoint = function () {
            $scope.selectedVisitPoint = {};
        }

        $scope.setTrackablesFromRawDataOfCalendar = function () {
            $scope.trackables = $scope.rawDataOfCalendar.trackables;
        }

        $scope.initDateTime = function () {
            $('#datetime').datetimepicker({
                format: 'YYYY-MM-DD HH:mm'
            });

            $('#datetime').on('dp.change', function (event) {
                $('#datetime').data("DateTimePicker").minDate(moment());
                $scope.selectedVisitPoint.datetime = $('#datetime').val();
                $scope.$apply();
            })

        }

        // Loaders
        $scope.initDateTime();
        $scope.loadCalendar();
        $scope.loadMarkers();
        $scope.modSchedHeight();
     }]);