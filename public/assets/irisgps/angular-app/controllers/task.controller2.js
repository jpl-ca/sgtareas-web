'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', []);

app.controller('taskController', function($scope, $http)
    {
    	 $scope.modSchedHeight = function (){
            var headHeight = 100;
            var sch = document.getElementById("scheduler_here");
            sch.style.height = (parseInt(document.body.offsetHeight)-headHeight)+"px";
        }
        $scope.init = function() {
            $scope.modSchedHeight();
            // window.resizeTo(950,700);
            scheduler.locale.labels.timeline_tab = "Timeline";
            scheduler.locale.labels.section_custom = "Section";
            scheduler.locale.labels.section_important = "Important";
            // scheduler.config.fix_tab_position = false;
            scheduler.config.details_on_create=true;
            scheduler.config.details_on_dblclick=true;
            scheduler.config.xml_date="%Y-%m-%d";


            //===============
            //Configuration
            //===============
            // scheduler.templates.event_class = function(start, end, event) {
            //     event.color = (event.important) ? "red" : "";
            //     return "";
            // };

            var agents = [
                {key: 1, label: "James Smith"},
                {key: 2, label: "John Williams"},
                {key: 3, label: "David Miller"},
                {key: 4, label: "Linda Brown"}
            ];

            var lugares = [
                {key: 1, label: "Lugar 1"},
                {key: 2, label: "Lugar 2"},
                {key: 3, label: "Lugar 3"},
                {key: 4, label: "Lugar 4"}
            ];
             
            scheduler.createTimelineView({
                name:   "timeline",
                x_unit: "day",
                x_date: "%d %l",
                x_step: 1,
                x_size: 7,
                x_start: 1,
                x_length: 7,
                y_unit: agents,
                y_property: "agent_id",
                render:"bar",
                round_position: true
            });
            // Working week
            scheduler.date.timeline_start = scheduler.date.week_start;
            scheduler.date.add_timeline = function(date, step, something) {
                return scheduler.date.add(date, step * 7, "day");
            };

            //===============
            //Data loading
            //===============
            // scheduler.config.lightbox.agents=[    
            // {name:"Descripción", height:130, map_to:"text", type:"textarea" , focus:true},
            // {name:"Agente", height:23, type:"select", options:agents, map_to:"lugar_id" },
            // {name:"custom2", height:23, type:"select", options:lugares, map_to:"lugar_id" },
            // {name:"time", height:72, type:"time", map_to:"auto", time_format:["%H:%i"] }
            // ]

            
            
            var block_id = null; 
        var startDate = new Date(); 
        scheduler.attachEvent("onBeforeViewChange", function(old_mode,old_date,mode,date){
            if(block_id)
            scheduler.deleteMarkedTimespan(block_id);   
            var from = scheduler.date[mode + "_start"](new Date(date));
            var to = new Date(Math.min(new Date(startDate), + scheduler.date.add(from, 0, mode)));
            // scheduler.config.drag_move = false;
            block_id = scheduler.addMarkedTimespan({
              start_date: from, 
              end_date:to,
              type:"dhx_time_block"
            });
              
            return true;
        });
        startDate.setDate(startDate.getDate()-1); 
        
        scheduler.config.limit_start = new Date(startDate);
        scheduler.config.limit_end = new Date(9999, 1,1);
        setInterval(function(){     
           scheduler.config.limit_start = new Date(startDate);
        }, 1000*60);
        scheduler.init('scheduler_here', new Date(), "timeline");
        // scheduler.load(base_url+"/resources/views/web/tasks/data.js","json");
            scheduler.parse([
            { start_date: "2016-06-06", end_date: "2016-06-06", ruta: "lol", text:"Task A-12458", checklist:[
            {name: "trabajar"},
            {name: "limpiar"},
            {name: "cobrar"}
            ], agent_id:1},
            { start_date: "2016-06-09", end_date: "2016-06-09", ruta: "lol2", text:"Task A-89411", checklist:[
            {name: "limpiar2"},
            {name: "cobrar2"}
            ], agent_id:1},
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol3", text:"Task A-64168", agent_id:1},
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol4", text:"Task A-46598", agent_id:1},
            { start_date: "2016-05-27", end_date: "2016-05-28", hora:'10pm',ruta: "lol5", text:"tarea xddd", agent_id:1},
            
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol6", text:"Task B-48865", agent_id:2},
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol7", text:"Task B-44864", agent_id:2},
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol8", text:"Task B-46558", agent_id:2},
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol9", text:"Task B-45564", agent_id:2},
            
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol10", text:"Task C-32421", agent_id:3},
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol11", text:"Task C-14244", agent_id:3},
            
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol12", text:"Task D-52688", agent_id:4},
            { start_date: "2016-05-26", end_date: "2016-05-26", hora:'10pm',ruta: "lol13", text:"Task D-46588", agent_id:4},
            { start_date: "2016-05-31", end_date: "2016-05-31", hora:'10pm',ruta: "lol14", text:"lool", agent_id:4}
            ], "json");
            
            // console.log(agents);
            // console.log(scheduler.getEvents());
        
         var html = function(id) { return document.getElementById(id); }; //just a helper

         scheduler.showLightbox = function (id) {
            var ev = scheduler.getEvent(id);
            scheduler.startLightbox(id, html("my_form2"));
            var star_date_ev = ev.start_date;
            var today = new Date();
            var today_f = today.toISOString().substring(0, 10);
            var start_date_f = star_date_ev.toISOString().substring(0, 10);
            html("text").value = ev.text;
            html("nruta").value = ev.ruta || "";
            html("hora").value = ev.hora || "";
            $scope.checklist = ev.checklist;
            $scope.$apply();
        };
        $scope.save_form = function () {
            var ev = scheduler.getEvent(scheduler.getState().lightbox_id);
            ev.text = html("text").value;
            ev.ruta = html("nruta").value;
            ev.hora = html("hora").value;
            ev.checklist = $scope.checklist;
            scheduler.endLightbox(true, html("my_form2"));

        }
         $scope.delete_event = function () {
            var event_id = scheduler.getState().lightbox_id;
            scheduler.endLightbox(false, html("my_form2"));
            scheduler.deleteEvent(event_id);
        }

         scheduler.attachEvent("onDblClick", function (id, e){
               var ev = scheduler.getEvent(id);
               var star_date_ev = ev.start_date;
               var today = new Date();
               var today_f = today.toISOString().substring(0, 10);
               var start_date_f = star_date_ev.toISOString().substring(0, 10);
               if(start_date_f < today_f){
                    scheduler.startLightbox(id, html("my_form"));   
                    // $scope.load_event(ev);                 
                    html("task").innerHTML  = ev.text;
                    html("ruta").innerHTML  = ev.ruta;                 
                    html("fecha").innerHTML  = start_date_f;
                    for (var i = 0; i <= ev.checklist.length; i++) {
                    var checklist = ev.checklist[i];
                    var texto = checklist.name;
                    var li = document.createElement('li');
                    li.innerHTML = texto ==''?'(nada)':texto;
                    document.getElementById('lista').appendChild(li);
                    };                        
               }
               return true;               
          });

        // t,r,chk,sd,ed,s_id
        // $scope.load_event = function (obj) {
        //     $scope.obj = obj;  
        //     alert($scope.obj.text);
        //     $scope.$watch('obj', function() {
        //         $scope.item = $scope.obj;  
        //     }, true);    
        // }
        
       $scope.ver = function() {
            console.log(scheduler._events);
       }

        $scope.borrar = function (){
                    var lis=document.getElementById('lista').getElementsByTagName('li');
                    for(var i=0; i<lis.length;i++){
                        lis[i].onclick=function(){
                            if(confirm('¿Borrar este li?'))
                            this.parentNode.removeChild(this);
                        };
                    }
                }
        $scope.close_form = function () {
            scheduler.endLightbox(false, html("my_form"));
            scheduler.endLightbox(false, html("my_form2"));
            $('#lista').empty();
        }
        $scope.checklist = [];
        $scope.addItemChecklist = function() {
            if($scope.checklist == undefined){
                $scope.checklist = [];
                $scope.checklist.push({name:$scope.itemlist});
                $scope.itemlist = '';
            }else{
                $scope.checklist.push({name:$scope.itemlist});
                $scope.itemlist = '';
            }
             
        }
        $scope.removeItemChecklist = function(index) {
             $scope.checklist.splice(index,1);
        }
    }
    $scope.init();

     });