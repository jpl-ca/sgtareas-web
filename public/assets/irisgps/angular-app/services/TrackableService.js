irisGpsApp.factory('TrackableService', function($http) {
    return {
        disconnectAsset: function (data) {
            return $http.post(baseUrl + '/api/trackables/disconnect', data).then(function (response) {
                return response.data;
            });
        },
        trackablesWithGeolocation: function () {
            return $http.get(baseUrl + '/api/trackables/geolocation').then(function (response) {
                return response.data;
            });
        }
    };
});