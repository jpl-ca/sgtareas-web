app.factory('reportsService', function($http) {
    return {
        getAll: function (start_date, end_date) {
            return $http.get(base_url +'/api/auth/organization/me/visit-points/items-grouped-by-day?startDate='+start_date+'&endDate='+end_date).then(function (response) {
                return response.data;
            });
        },
        getAgents: function () {
            return $http.get(base_url +'/api/agents').then(function (response) {
                return response.data;
            });
        },
        getAllforAgent: function (id, start_date, end_date) {
            return $http.get(base_url +'/api/auth/organization/me/trackables/agent/'+ id +'/visit-points-report?startDate='+start_date+'&endDate='+end_date).then(function (response) {
                return response.data;
            });
        },
        getVisits: function (id, start_date, end_date) {
            return $http.get(base_url +'/api/auth/organization/me/tasks/calendar?taskable_id='+ id +'&startDate='+start_date+'&endDate='+end_date).then(function (response) {
                return response.data;                
            });
        },
        getDistanceTraveledOfAllAgents: function(startDate, endDate) {
            return $http.get(base_url +'/api/auth/organization/me/reports/total-distance-traveled-of-all-agents?startDate=' + startDate + '&endDate=' + endDate).then(function (response) {
                return response.data;                
            });
        }
    };
});