$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();

    var toastrAlerts = $('body').find('.toastr');

    toastrAlerts.each(function () {
        var type = $(this).data("type");
        var message = $(this).data("message");
        displayToastr("toast-bottom-full-width", type, message);
    });

    $(".confirm").confirm();

    $(".confirm-post").confirm({
        post:true
    });

     /* ventana de sugerencias*/

    var html = function(id) { return document.getElementById(id); };

    $('#suggestion').click(function() {
        $('#form_suggestion').show();
        $('#success-message').hide();
        $('#box_suggestion').fadeIn();  
        html("content-message").value = " ";
    });

    $('#cancel').click(function() {
        $('#box_suggestion').hide();    
    });

    /**/
    var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
    
     $('#form_suggestion').submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var postdata = form.serialize();
        $.ajax({
            type: 'POST',
            url: base_url+'/api/suggestions',
            data: postdata,
            dataType: 'json',
            success: function(json) {
                    form.hide();
                    $('#success-message').fadeIn();
                    setTimeout(function () {
                    $('#box_suggestion').hide();
                        }, 3000); 
            }
        });
    });

});

$(document).on('ready', function () {
    $('.input-date').datetimepicker({
        format: 'YYYY-MM-DD'
    });
});

var baseUrl = window.location.origin;

jQuery(document).ready(function() {

     var path = window.location.pathname;
     console.log(path);
            if (/dashboard/.test(path)) {
                 $("#item1").addClass("active");
            }
            if (/trackable-assets/.test(path)) {
                 $("#item2").addClass("active");
            }
            if (/tasks/.test(path)) {
                 $("#item3").addClass("active");
            }
            if (/markers/.test(path)) {
                 $("#item4").addClass("active");
            }
            if (/users/.test(path)) {
                 $("#item5").addClass("active");
            }
            if (/organizations/.test(path)) {
                 $("#item7").addClass("active");
            }
            if (/suggestions/.test(path)) {
                 $("#item8").addClass("active");
            }         

});