@extends('layouts.app')
@include('partials.landing.navbar')
@section('body')
<style>
body{
background:url('/assets/irisgps/img-web/foto_portada.jpg')no-repeat left top;
background-size: cover;   	
}
 
</style>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
         <!--    {!! Html::pageHeader("Landing Page!") !!} -->
    	</div>
    	<div class="col-md-5 col-sm-5 col-xs-12">
    	</div>
    	<div class="col-md-7 col-sm-7 col-xs-12 desc">
    		<h1 class="landing-tit">¡Tu negocio, mucho más competitivo!</h1>
            <a href="{{ url('/register') }}">
                <div class="box-free">
                    <h2>¡UN MES GRATIS!</h2>
                    <h3>PRUÉBALO AHORA</h3>
                </div>
            </a>
            <ul>
                <li>Registro tus clientes, sin límites y para siempre.</li>
                <li>Programa tareas y  visitas a tus clientes ilimitadamente.</li>
            </ul>
    		<!-- <p>SGTareas simplifica la gestión y asignación de tareas a ti y tu equipo de trabajo. Crear y mantiene tu lista de clientes.</p> -->
    	</div>
        <div class="container">
            <div class="col-lg-9 col-md-9 col-sm-7 col-xs-7"></div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3">
                <a href="https://play.google.com/store/apps/details?id=com.sgtel.sgtareas" target="_blank">
                <img class="play" src="{{ url('assets/irisgps/img-web/descarga.png') }}" alt="disponible en google play">
                </a>
            </div>
        </div>
</div>


<footer style="padding-left: 50px;position: absolute;">
    <p  class='text-left'>
        Desarrollado por <a href="http://sgtel.pe/" target="_blank" style="color:#05498a;font-weight:bold;"> 
    SGTEL</a> © 2016 Todos los Derechos Reservados
    </p>
    <p class="text-right" style="float: right;">
        <a href="http://sgtel.pe/" target="_blank">
            <img src="{{ url('assets/irisgps/img-web/sgtel-mini.png')}}" alt="">
        </a>
    </p>
</footer>
@endsection
