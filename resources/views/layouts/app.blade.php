<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{url('assets/irisgps/img-web/icon.png')}}">
    <title>
        @section('title')
            SGTareas
        @show
    </title>

    <!-- Fonts -->
    {!! Html::fontAwesomeAsset() !!}

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    {!! Html::bootstrapAsset('css', '3.3.6', 'paper') !!}
    {!! Html::style('assets/irisgps/css/irisgps.css') !!}
    {!! Html::style('assets/toastr/css/toastr.min.css') !!}
    {!! Html::style('assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}

    @yield('head')
</head>
<body
      @yield('body-tag')
>
    @yield('body')
    <!-- JavaScripts -->
    {!! Html::jqueryAsset() !!}
    {!! Html::bootstrapAsset('js') !!}
    {!! Html::script('assets/jquery.confirm/jquery.confirm.min.js') !!}
    {!! Html::script('assets/toastr/js/toastr.min.js') !!}
    {!! Html::script('assets/irisgps/js/alerts.js') !!}
    {!! Html::script('assets/irisgps/js/helpers.js') !!}
    {!! Html::script('assets/irisgps/js/irisgps.js') !!}
    {!! Html::script('assets/moment/moment.min.js') !!}
    {!! Html::script('assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script>
       baseUrl = "{{ url('')}}";
    </script>
    @yield('javascript')
    @if (auth()->check())
    <div class="suggestion" id="suggestion">
        <img src="{{ url('assets/irisgps/img-web/suggestion.png')}}" alt="sugerencias">
    </div>
    @endif
    

    <div class="box_suggestion" id="box_suggestion">
        <div class="head">
            <p>¿Tienes alguna sugerencia? <label> Envíanosla</label></p>
        </div>
        <form id="form_suggestion">
        <div class="body">
            <div class="form-group">
                <textarea name="content" class="form-control content-message" id="content-message">
                </textarea>                
            </div>
        </div>
        <div class="footer">            
            <input type="submit" value="ENVIAR" class="btn btn-success pull-right">
            <a class="btn btn-default pull-right margin-r-15" id="cancel">CERRAR</a>
        </div>
        </form>
        <p id="success-message">Sugerencia registrada exitosamente</p>
    </div>
</body>
</html>
