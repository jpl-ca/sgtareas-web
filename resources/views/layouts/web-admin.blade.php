@extends('layouts.app')

@section('body')
    <div class="web-container">
        @include('partials.web.navbar-admin')
        @include('partials.alerts')
        @yield('content')
    </div>
@endsection