@extends('layouts.app')
@section('body')
    <div class="web-container">
        @include('partials.web.navbar')
        @include('partials.alerts')
        @yield('content')
    </div>
    @include('partials.web.footer')
@endsection