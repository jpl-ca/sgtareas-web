<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/dashboard') }}">
                <img class="logo-sg" src="{{ url('/assets/irisgps/img-web/logo.png') }}" alt="logo">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @if (Auth::check())
                    <li><a id="item1" href="{{ action('Web\DashboardController@index') }}">Monitor</a></li>

                    @if (auth()->user()->hasPrivilege(['create-trackable-agent', 'store-trackable-agent', 'view-trackable-agent', 'edit-trackable-agent', 'destroy-trackable-agent']))
                    <li><a id="item2" href="{{ action('Web\TrackableController@index') }}">Agentes</a></li>
                    @endif

                    @if(auth()->user()->hasPrivilege('manage-tasks'))
                    <li><a id="item3" href="{{ action('Web\TaskController@calendar') }}">Programación</a></li>
                    @endif
                    
                    @if (auth()->user()->hasPrivilege(['create-marker', 'store-marker', 'view-marker', 'edit-marker', 'destroy-marker', 'destroy-marker']))
                    <li><a id="item4" href="{{ action('Web\MarkerController@index') }}">Clientes</a></li>
                    @endif
                    
                    @if (auth()->user()->hasPrivilege(['create-user', 'store-user', 'view-user', 'edit-user', 'destroy-user']))
                    <li><a id="item5" href="{{ action('Web\UserController@index') }}">Usuarios</a></li>
                    @endif

                    @if(auth()->user()->hasPrivilege('manage-reports'))
                    <li class="dropdown">
                        <a id="item6"  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Reportes  <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ action('Web\ReportController@index') }}">Reporte de Visitas</a></li>
                            <li><a href="{{ action('Web\ReportController@a') }}">Reporte de Visitas por Agente</a></li>
                            <li><a href="{{ action('Web\ReportController@totalDistanceTraveledOfAllAgentsOfOrganization') }}">Reporte de la Distancia recorrida por agente</a></li>
                        </ul>
                    </li>
                    @endif
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <!-- <li><a href="{{ url('/login') }}">Login</a></li> -->
                    <li><a href="{{ url('/register') }}">¿Aún no tienes una cuenta? <r class="signup">Registrate</r></a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding-top: 10px;">
                          <div style="font-weight: bold; text-transform: uppercase;">
                        {{ Auth::user()->organization->name }} <span class="caret"></span></div>
                          <div style="text-align: right;font-size: 0.85em;position: absolute;right: 30px; margin-top:-4px; text-transform: capitalize; width: 150px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
                            <i class="fa fa-user"></i> {{Auth::user()->name }}
                          </div>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ action('Web\AccountController@index') }}"><i class="fa fa-btn fa-user"></i>Mi cuenta</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Salir</a></li>

                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>