@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
	<div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
                {!! Html::pageHeader("Mi cuenta") !!}
                <div class="row margin-bottom-20">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Detalle</a></li>
                            <li role="presentation"><a href="#organization" aria-controls="organization" role="tab" data-toggle="tab">Organización</a></li>
                            <li role="presentation"><a href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab">Suscripción</a></li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="info">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include('web.users.partials.information')
                                <a class='btn btn-primary' href="{{ action('Web\AccountController@edit') }}">Cambiar datos</a>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="organization">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                {!! Form::fhText('ruc', 'RUC', $user->organization->ruc, ['readonly' => true]) !!}
                                {!! Form::fhText('organization_name', 'Nombre', $user->organization->name, ['readonly' => true]) !!}
                                {!! Form::fhText('organization_code', 'Código', $user->organization->id, ['readonly' => true]) !!}
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="subscription">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                {!! Form::fhText('subscription_plan', 'Plan', $user->organization->subscriptionPlan()->name, ['readonly' => true]) !!}
                                {!! Form::fhText('description', 'Expira', carbonFormat($user->organization->subscription_ends), ['readonly' => true]) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection