@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
            
                <div class="row">
                    <h2>Su cuenta de organización ha sido bloqueada</h2>
                    <p>Contáctese con nuestro equipo de soporte para obtener más información<p>
                </div>
            </div>
        </div>
    </div>
@endsection
