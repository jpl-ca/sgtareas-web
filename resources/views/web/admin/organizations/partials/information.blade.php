<div class="row margin-bottom-20">
    <div class="col-md-12">
		{!! Form::fhText('ruc', 'RUC', $organization->ruc, ['readonly' => true]) !!}

        {!! Form::fhText('name', 'Nombres', $organization->name, ['readonly' => true]) !!}

        {!! Form::fhText('subscription_id', 'Tipo de suscripción', $organization->subscription_id, ['readonly' => true]) !!}

        {!! Form::fhText('subscription_ends', 'Suscripción finaliza', $organization->subscription_ends, ['readonly' => true]) !!}
    </div>
</div>