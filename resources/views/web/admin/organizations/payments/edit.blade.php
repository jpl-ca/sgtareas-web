@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 card-box padding-30 col-md-offset-2">
                <h2 class="page-header">
                    <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                    Edición de Pago
                    <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>

{!! Form::open(['url' => action('Web\Admin\OrganizationPaymentController@update', ['organization_id' => $item->organization_id, 'itemId' => $item->id]), 'method' => 'POST', 'files' => true]) !!}

                {!! Form::token() !!}
                
                @if(isset($nextUrl) && $nextUrl)
                {!! Form::hidden('nextUrl', $nextUrl) !!}
                @endif

                {!! Form::fhText('monto', 'Monto', $item->amount, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('date', 'Fecha de pago', $item->date, ['autocomplete' => 'off', 'class' => 'input-date form-control']) !!}

                {!! Form::fhText('subscription_start_date', 'Inicio de suscripción', $item->subscription_start_date, ['autocomplete' => 'off', 'class' => 'input-date form-control']) !!}

                {!! Form::fhText('subscription_end_date', 'Fin de suscripción', $item->subscription_end_date, ['autocomplete' => 'off', 'class' => 'input-date form-control clearfix']) !!}
                
                {!! Form::fhText('additional_information', 'Información adicional', $item->additional_information) !!}

                <div class="clearfix">
                        <label for="image" class='col-md-4'>Imagen</label>
                        <div class='col-md-6'>
                            {!! Form::file('image') !!}
                        </div>
                </div>

                {!! Form::fhSubmit('Actualizar', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection