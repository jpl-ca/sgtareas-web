@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">

                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li><a href='/admin/organizations'>Organizaciones</a></li>
                  <li><a href="{{ action('Web\Admin\OrganizationController@view', ['organization_id' => $organization->id]) }}">{{ $organization->name }}</a></li>
                  <li class='active'>Pagos</li>
                </ol>

                <h2 class="page-header">
                    <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                    Pagos
                </h2>
                    
                    <nav class="navbar navbar-default">
                    <!--Begin button-->
                    <a href="{{ action('Web\Admin\OrganizationPaymentController@create', ['organization_id' => $organization->id]) }}" class='btn btn-primary pull-right margin-right-15 margin-top-15'>Registrar pago</a>
                    <!--End button-->

                    
                      <div class="container">
                        <form class="navbar-form navbar-left" role="search" method='GET' action='{{Request::url()}}'>
                            <div class="form-group">
                                {{ Form::text('search_text', (isset($searchText) ? $searchText : ''), ['class' => 'form-control', 'placeholder' => 'Ingrese texto a buscar']) }}
                            </div>
                            <div class='form-group'>
                                {{ Form::select('search_type', (isset($searchTypes) ? $searchTypes : []), (isset($searchType) ? $searchType : null), ['class' => 'form-control']) }}
                            </div>
                            <button type="submit" class="btn btn-default">Buscar</button>
                        </form>
                      </div>
                    </nav>
                    @include('web.admin.organizations.payments.partials.table')
                    {{ $items->render() }}

            </div>
        </div>
    </div>
@endsection