@extends('layouts.web-admin')

@section('content')
<style>
  footer{
    position: absolute !important;
  }
</style>
    <div class="container">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">

                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li><a href="/admin/organizations">Organizaciones</a></li>
                  <li><a href="/admin/organizations/{{ $organization->id }}">{{ $organization->name}}</a></li>
                  <li><a href="/admin/organizations/{{ $organization->id }}/users">Usuarios</a></li>
                  <li class='active'>Ver</li>
                </ol>

                <h2 class="page-header">
                  <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                  Usuario
                  <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>

                <div class="row margin-bottom-20">
                    <div class="col-md-12">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Información</a></li>
                        </ul>

                    </div>
                </div>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="info">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include('web.admin.organizations.users.partials.information')
                                @if(isset($hasPrivilegesToEdit) && $hasPrivilegesToEdit)
                                <a href="{{ action('Web\Admin\OrganizationUserController@edit', ['organization_id' => $user->organization->id, 'user' => $user]) }}" class="btn btn-primary margin-bottom-20">Editar</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include('web.includes.google-maps')
    {!! Html::script('assets/irisgps/js/maps.js') !!}
    {!! Html::script('assets/irisgps/js/geolocation.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('a[data-toggle="tab"][href="#geolocation"]').on('shown.bs.tab', function (e) {
                initGeolocation();
            });
        });
    </script>
@endsection
