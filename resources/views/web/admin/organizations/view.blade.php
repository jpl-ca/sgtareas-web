@extends('layouts.web-admin')

@section('content')
    <div class="container">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li><a href="/admin/organizations">Organizaciones</a></li>
                  <li class='active'>{{ $organization->name}}</li>
                </ol>

                <h2 class="page-header">
                  <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                  Organización
                  <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>
                
                <div class="row margin-bottom-20">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Detalle</a></li>
                            <li role="presentation"><a href="{{ url("/admin/organizations/{$organization->id}/users") }}" aria-controls="users" role="tab">Operadores</a></li>
                            <li role="presentation"><a href="{{ url("/admin/organizations/{$organization->id}/payments") }}" aria-controls="pagos" role="tab">Pagos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="info">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include('web.admin.organizations.partials.information')
                                <a href="{{ action('Web\Admin\OrganizationController@edit', $organization) }}" class="btn btn-primary margin-bottom-20">Editar</a>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="users">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="subscription">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include('web.includes.google-maps')
    {!! Html::script('assets/irisgps/js/maps.js') !!}
    {!! Html::script('assets/irisgps/js/geolocation.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('a[data-toggle="tab"][href="#geolocation"]').on('shown.bs.tab', function (e) {
                initGeolocation();
            });
        });
    </script>
@endsection
