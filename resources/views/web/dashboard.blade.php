@extends('layouts.web')

@section('head')
@endsection

@section('body-tag')
    ng-app="irisGpsApp"
@endsection

@section('content')
<style>
footer{
    display: none;
}
#mapOptionsModal, #mapOptionsButton{
    display: none;
}
.legend{
    width: 150px;
    height: 119px;
    background: rgba(255,255,255,0.8);
    left: 0px;
    bottom: 35px;
    position: fixed;
    z-index: 100;
}
.legend p{
    font-size: 15px;
    margin-bottom: 0px;
    text-align: center;
}
.legend img{
    width: 18px;
    margin-right: 5px;
}
.legend ul{
    padding-left: 10px;
    list-style: none;
}
</style>
    <div class="container-fluid" ng-controller="dashboardController">
        <div class="legend">
            <p>Leyenda</p>
            <ul>
                <li><img src="{{ url('assets/irisgps/icons/agents/active.png') }}" alt="">Activo</li>
                <li><img src="{{ url('assets/irisgps/icons/agents/inactive.png') }}" alt="">Inactivo</li>
                <li><img src="{{ url('assets/irisgps/icons/agents/disconnected.png') }}" alt="">Desconectado</li>
            </ul>
        </div>
        <div class="row">
            <div id="map-dashboard" class="full-width-div"></div>
            <div class="hidden map-markers" id="map-markers">
                <div ng-repeat="trackable in trackables" data-role="marker" data-asset-type="@{{ trackable.type }}" data-lat="@{{ trackable.last_lat }}" data-lng="@{{ trackable.last_lng }}" data-title="@{{ trackable.full_name }}" data-status="@{{ trackable.state  }}" data-icon="@{{ trackable.icon }}" data-info-window="@{{ trackable.info_window }}" marker></div>
            </div>
            <div class="hidden" id="configuration" data-refresh-inverval=""></div>
        </div>

        <div class="modal fade" id="mapOptionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="padding-bottom: 0px">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="mapOptionsModalLabel">Opciones de mapa</h4>
                    </div>
                    <div class="modal-body" style="padding-top: 0px">
                        <h6 class="page-header">Mostrar</h6>
                        <div class="radio">
                            <label>
                                <input type="radio" name="mapDisplay" ng-model="mapDisplay" value="all" ng-click="changeMapDisplayOptions()">
                                Todo
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="mapDisplay" ng-model="mapDisplay" value="agents" ng-click="changeMapDisplayOptions()">
                                Solo agentes
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="mapDisplay" ng-model="mapDisplay" value="vehicles" ng-click="changeMapDisplayOptions()">
                                Solo vehículos
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="mapOptionsButton" style="padding: 15px; z-index: 0; position: absolute; bottom: 89px; right: 0px;">
            <a href="javascript:void(0)" class="float-btn btn-primary" ng-click="displayMapOptionsModal()">
                <i class="fa fa-cog fa-fw">
                    <div class="share-ripple ripple-wrapper"></div>
                </i>
            </a>
        </div>

    </div>
@endsection

@section('javascript')
    @include('web.includes.google-maps')
    {{ Html::script('assets/angular/angular.min.js') }}
    {{ Html::script('assets/irisgps/js/maps.js') }}
    {{ Html::script('assets/irisgps/angular-app/angular.irisgps.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/TrackableService.js') }}
    <script>

        function loadMarker(scope, element) {

            var lat = parseFloat(element.lat);
            var lng = parseFloat(element.lng);
            var title = element.title;
            var icon = iconBase() + element.icon;
            var status = element.status;
            var infoWindow = element.infoWindow;
            var type = element.assetType;

            var marker = createMarker({lat: lat, lng: lng}, title, icon);
            setInfoBoxOnMarker(scope.map, marker, infoWindow);

            scope.markers.push(marker);

            switch (status) {
                case 'active':
                    scope.active.push(marker);
                    break;
                case 'inactive':
                    scope.inactive.push(marker);
                    break;
                case 'disconnected':
                    scope.disconnected.push(marker);
                    break;
            }

            if (typeof type != "undefined") {
                switch (type) {
                    case 'agent':
                        scope.types.agents.push(marker);
                        break;
                    case 'vehicle':
                        scope.types.vehicles.push(marker);
                        break;
                }
            }
            /*
            setMarkerOnMap(scope.map, scope.active);
            setMarkerOnMap(scope.map, scope.inactive);
            setMarkerOnMap(scope.map, scope.disconnected);
            */
            setMarkerOnMap(scope.map, scope.markers);
            displaySelected(scope);
        }

        function displaySelected(scope) {
            var selected = [];
            switch (scope.mapDisplay) {
                case 'all':
                    selected = scope.markers;
                    break;
                case 'agents':
                    selected = scope.types.agents;
                    break;
                case 'vehicles':
                    selected = scope.types.vehicles;
                    break;
            }
            removeMarkerFromMap(scope.markers);
            setMarkerOnMap(scope.map, selected);
        }

        irisGpsApp.directive('marker', function(){
            return {
                link: function(scope, elm){
                    loadMarker(scope, arguments[2]);
                }
            }
        });

        irisGpsApp.controller('dashboardController', ['$scope', '$compile', '$window', 'TrackableService', function($scope, $compile, $window, TrackableService) {

            $scope.trackables = null;

            $scope.map;

            $scope.markers = [];
            $scope.active = [];
            $scope.inactive = [];
            $scope.disconnected = [];
            $scope.types = {
                agents: [],
                vehicles: []
            };

            $scope.mapDisplay = 'all';

            $scope.displayMapOptionsModal = function() {
                $('#mapOptionsModal').modal();
            };

            $scope.changeMapDisplayOptions = function() {
                displaySelected($scope);
            };

            var styles = defaultMapStyle();

            function initGeolocation() {
                if ($scope.map == null) {
                    $scope.map = initMap('map-dashboard');
                    setMapStyles($scope.map, styles);
                }
            }

            initGeolocation();
            $scope.map.setZoom(12);
            $scope.map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('mapOptionsButton'));
            refreshMap();


            function refreshMap() {
                TrackableService.trackablesWithGeolocation().then(function(trackables){
                    removeMarkerFromMap($scope.markers);
                    clearAllArrays();
                    $scope.trackables = trackables;
                    $scope.$apply;
                    displaySelected($scope);
                });
            }

            function clearAllArrays() {
                $scope.trackables = [];
                $scope.markers = [];
                $scope.active = [];
                $scope.inactive = [];
                $scope.disconnected = [];
                $scope.types = {
                    agents: [],
                    vehicles: []
                };
            }

            window.setInterval(function(){
                refreshMap();
                console.log('map refreshed');
            }, 10000);

        }]);
    </script>
@endsection
