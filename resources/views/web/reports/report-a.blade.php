@extends('layouts.web')

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
<style>
    #scheduler_here {
    width   : 100%;
    height  : 600px;
}                                   
.modal-content {
    overflow-y: scroll;
}     
.options{
    padding-bottom: 15px;
}        
.dhx_cal_event_clear {
    height: 18px !important;
}             
footer{
    margin-top: 83px;
} 
</style>
    <div class="container-fluid" ng-controller="ReportsController" ng-init="init()">
        <div class="container  margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Reporte de Visitas por Agente</h2>
                <div>                    
                    <nav class="navbar navbar-default">
                        <div class="dropdown pull-right margin-top-15 margin-right-15">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Exportar
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li><a href="#" ng-click="exportScheduler('pdf')">PDF</a></li>
                              <li><a href="#" ng-click="exportScheduler('xls')">XLS</a></li>
                            </ul>
                        </div>
                        <div class="container">
                          <form class="navbar-form navbar-left" role="search">
                              <div class="form-group">
                                  <label for="">Agente: </label>
                                  <select class="form-control" id="selected_agent" name="agent" 
                          		ng-options="agent as agent.first_name + ' ' + agent.last_name  for agent in agents track by agent.first_name" ng-model="selected_agent">
                          </select>
                              </div>
                              <!-- <div class="form-group">
                                  <label for="">Desde: </label>
                                  <input class="form-control" id="start_date" placeholder="YYYY-MM-DD" autocomplete="off" ng-model="start_date" required="1" name="start_date" type="text">
                              </div>
                              <div class="form-group">
                                  <label for="">Hasta: </label>
                                  <input class="form-control" id="end_date" placeholder="YYYY-MM-DD" autocomplete="off" ng-model="end_date" required="1" name="end_date" type="text">
                              </div> -->
                              <div class="form-group hide">
                                  <label for="">Mes: </label>
                                  <input class="form-control" id="month" placeholder="YYYY-MM-DD" autocomplete="off" ng-model="month" required="1" name="month" type="text">
                              </div>

                              <div class='form-group'>
                                <label for="">Año:</label>
                                <select class='form-control' name="selectedYear" ng-model="selectedYear">
                                    <option value="">Seleccione año</option>
                                    <option ng-repeat='year in listOfYears'>@{{year}}</option>
                                  </select>
                              </div>

                              <div class='form-group'>
                                <label for="">Mes:</label>
                                <select class='form-control' name="selectedMonth" ng-model="selectedMonth">
                                    <option ng-repeat='month in listOfMonths' value="@{{month.key}}">@{{month.label}}</option>
                                  </select>
                              </div>

                              <button type="submit" class="btn btn-default" ng-click="getVisitsforAgent()">Buscar</button>
                          </form>                        
                        </div>
                    </nav>
                    <br>
                    <p class="text-center tselect" ng-if="vpoints == ''">No se encontraron registros.</p>  
                    <p class="text-center tselect" ng-if="vpoints == undefined">Seleccione un agente y un mes.</p>
                     <div id="scheduler_here" class="dhx_cal_container">
                        <div class="dhx_cal_navline">
                            <!-- <div class="dhx_cal_prev_button">&nbsp;</div>
                            <div class="dhx_cal_next_button">&nbsp;</div>
                            <div class="dhx_cal_today_button">Hoy</div> -->
                            <div class="dhx_cal_date"></div>                            
                            <div style="left:0px; top: 17px;" class=''>
                                <ul class='list-inline list-visit-states-labels'>
                                    <li class='scheduled'>Programado</li>
                                    <li class='rescheduling-request'>Reprogramado</li>
                                    <li class='done'>Terminado</li>
                                </ul>
                            </div>
                        </div>
                        <div class="dhx_cal_header">
                        </div>
                        <div class="dhx_cal_data">
                        </div>      
                    </div>
                    <div class="modal fade" tabindex="-1" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title"> <i class='fa fa-map'></i> Información de visita</h4>
                          </div>
                          <div class="modal-body">
                                <div id="eventForm"><!--begin my_form2-->
                                    <form ng-submit="false">
                                    
                                    <div class='row'>
                                        <label for="text" class='col-md-3 col-sm-3 col-xs-3'>Agente </label>
                                        <input readonly="readonly" type="text" class='col-md-8 col-sm-8 col-xs-8' ng-model="selectedVisitPoint.selectedTrackable">
                                    </div>

                                    <div class='row'>
                                        <label for="text" class='col-md-3 col-sm-3 col-xs-3'>Cliente </label>
                                        <input readonly="readonly" type="text" class='col-md-8 col-sm-8 col-xs-8' ng-model="selectedVisitPoint.selectedMarker">
                                    </div>
                                    
                                    <div class='row'>
                                        <label for="time" class='col-md-3 col-sm-3 col-xs-3'>Fecha </label>
                                        <input  readonly="readonly" type="text" name="datetime" value="" id="datetime" class='col-md-8 col-sm-8 col-xs-8' ng-model="selectedVisitPoint.datetime"/>
                                    </div>

                                    <div class='row hide'>
                                        <label for="time" class='col-md-3 col-sm-3 col-xs-3'>Hora </label>
                                        <input  readonly="readonly" type="time" name="time" value="" id="time" class='col-md-8 col-sm-8 col-xs-8'/>
                                    </div>

                                    <div class="list">
                                        <h5> <i class='fa fa-list'></i> Lista de Tareas</h5>                                        
                                        <!--Checklist-->
                                        <ul class='list-group'>
                                            <li class='list-group-item' ng-repeat="item in selectedVisitPoint.checklist">                                                
                                                <label>@{{item.description}}</label>
                                            </li>
                                        </ul><!--End checklist-->

                                    </div>                
                                    <div class="options text-right">
                                        <input class='btn 'type="button" name="delete" value="Cerrar" id="delete" style='width:100px;' ng-click="hideSelectedVisitPointModal()">
                                    </div>
                                    </form>
                                </div><!--End my form2 -->



                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->


                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/calendarReportCtrl.js')}}
    {{ Html::script('assets/irisgps/angular-app/services/organization/reportsServices.js')}}
    {{ Html::script('assets/codebase/dhtmlxscheduler.js')}}
    {{ Html::script('assets/codebase/api.js')}}
    {{ Html::script('assets/codebase/locale/locale_es.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_timeline8c94.js?v=4.1.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_limit.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_serialize.js')}}
    {!! Html::style('assets/codebase/dhtmlxscheduler_flat.css') !!}
    {!! Html::style('assets/irisgps/css/calendar.css') !!}
@endsection