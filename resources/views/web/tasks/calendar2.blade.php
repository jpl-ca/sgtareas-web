@extends('layouts.web')

@section('head')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/task.controller2.js')}}
    {{ Html::script('assets/codebase/dhtmlxscheduler.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_timeline8c94.js?v=4.1.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_limit.js')}}
    {{ Html::script('assets/codebase/ext/dhtmlxscheduler_serialize.js')}}
    {!! Html::style('assets/codebase/dhtmlxscheduler_flat.css') !!}
@endsection

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {!! Html::pageHeader("Rutas") !!}
                <div  ng-controller="taskController">
                
                    <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
                        <div class="dhx_cal_navline">
                            <div class="dhx_cal_prev_button">&nbsp;</div>
                            <div class="dhx_cal_next_button">&nbsp;</div>
                            <div class="dhx_cal_today_button"></div>
                            <div class="dhx_cal_date"></div>
                            <div class="dhx_cal_tab" name="day_tab" style="left:30px;"></div>
                            <div class="dhx_cal_tab" name="timeline_tab" style="left:105px;"></div>
                        </div>
                        <div class="dhx_cal_header">
                        </div>
                        <div class="dhx_cal_data">
                        </div>      
                    </div>

                    <div id="my_form">
                        <img class="close" src="{{ url('assets/irisgps/icons/calendar/close.png') }}" ng-click="close_form()"/>
                        <div class="cont">
                             <!-- <input type="text" ng-model="item.text"> -->
                            Tarea:<label id="task"></label><br>
                            Ruta: <label id="ruta"></label><br>
                            Fecha: <label id="fecha"></label><br>
                            <ul id="lista"></ul>
                            <!-- <p ng-repeat="item in lista.checklist">CheckList: <input type="text" ng-model="item.name"></p>
                            </div> -->
                        </div>
                    </div>
                    <div id="my_form2">
                        <label for="text">text </label><input type="text" name="text" value="" id="text"><br>
                        <label for="nruta">Ubicación </label><input type="text" name="nruta" value="" id="nruta"><br>
                        <label for="hora">hora </label><input type="time" name="hora" value="" id="hora"><br><br>
                        <div class="checklist">
                            <input type="text" ng-model="itemlist">
                            <button ng-click="addItemChecklist()">Agregar Item</button>    
                        </div>
                        <div class="list">
                            <h5>Lista de Tareas</h5>
                            <hr>
                            <ul>
                                <li id="lista2" ng-repeat="item in checklist">
                                <label>@{{item.name}}</label>
                                <button ng-click="removeItemChecklist($index)"><i class="fa fa-times"></i></button>
                                </li>
                            </ul>
                        </div>
                        <br><br>
                        <div class="options">
                            <input type="button" name="save" value="Guardar" id="save" style='width:100px;' ng-click="save_form()">
                            <input type="button" name="close" value="Cerrar" id="close" style='width:100px;' ng-click="close_form()">
                            <input type="button" name="delete" value="Eliminar" id="delete" style='width:100px;' ng-click="delete_event()">
                        </div>
                    </div>
                    <button ng-click="ver()">Ver Array</button>
            </div>

        </div>
    </div>
@endsection