@extends('layouts.web')

@section('head')
    {!! Html::script('assets/angular/angular.min.js') !!}
@endsection

@section('body-tag')
    ng-app="irisGpsApp"
@endsection

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                {!! Html::pageHeader("Nueva ruta") !!}

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ action('Web\TaskController@index') }}"
                           class="btn btn-default pull-left margin-bottom-20">
                            Ir atrás
                        </a>
                    </div>
                </div>
                <div ng-controller="taskController">

                    <div class="modal fade" id="newMarkerModal" tabindex="-1" role="dialog" aria-labelledby="newMarkerModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="newMarkerModalLabel">Nuevo destino</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" role="form" action="#" method="POST">
                                        <div class="row">
                                            <div class="col-md-5">
                                                @include('web.tasks.markers.partials.creation-form')
                                            </div>
                                            <div class="col-md-7">
                                                <div id="map-new-marker"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary" ng-click="addNewMarker()">
                                        <i class="fa fa-check"></i>
                                        Crear nueva ubicación
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal fade" id="checklistModal" tabindex="-1" role="dialog" aria-labelledby="checklistModalLabel">
                        <div class="modal-dialog modal-md" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="checklistModalLabel">Listado de @{{ selectedChecklist.name }}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <form class="form-horizontal" role="form" ng-submit="addItemToChecklist(selectedChecklist.id)">
                                                <div class="form-group">

                                                    <div class="col-md-8">
                                                        <input class="form-control" autocomplete="off" placeholder="Descripción del elemento" ng-model="newItem.description" name="item_description" type="text" required>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <button type="submit" class="btn btn-primary btn-sm">Agregar nuevo</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5 class="page-header">Lista</h5>
                                            <ul ng-show="selectedChecklist.checklist.length">
                                                <li ng-repeat="item in selectedChecklist.checklist">@{{ item.description }} <a href="javascript:void(0)" class="btn btn-default btn-xs text-danger" ng-click="removeItemFromChecklist($index)"><span class="fa fa-times"></span></a> </li>
                                            </ul>
                                            <p class="text-center" ng-hide="selectedChecklist.checklist.length">No hay elementos...</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" ng-click="closeChecklist()">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <form name="taskForm" class="form-horizontal" role="form" ng-submit="createTask()">

                        <div class="row margin-bottom-20">
                            <div class="col-md-6 margin-bottom-20" style="display: none;">
                                <div id="map"></div>
                                <div class="hidden map-markers" id="map-markers">
                                </div>
                            </div>
                            <div class="col-md-6 margin-bottom-20">
                                <h3 class="page-header">Detalle</h3>
                                {!! Form::token() !!}

                                {!! Form::fhText('start_date', 'Inicia', null, ['id' => 'start_date', 'placeholder' => 'YYYY-MM-DD HH:mm', 'autocomplete' => 'off', 'ng-model' => 'task.start_date', 'required' => true]) !!}

                                {!! Form::fhText('end_date', 'Finaliza', null, ['id' => 'end_date', 'placeholder' => 'YYYY-MM-DD HH:mm', 'autocomplete' => 'off', 'ng-model' => 'task.end_date', 'required' => true]) !!}

                                {!! Form::fhText('description', 'Descripción', null, ['autocomplete' => 'off', 'ng-model' => 'task.description', 'required' => true]) !!}

                                <div class="form-group">

                                    <label for="assigned_to" class="control-label col-md-4">Asignado a</label>

                                    <div class="col-md-6">
                                        <select class="form-control" name="assigned_to" ng-model="task.assigned_to" required>
                                            <option ng-repeat="agent in agents" value="@{{'IrisGPS\\Agent-' + agent.id}}">@{{ 'Agent: ' + agent.first_name + ' ' + agent.last_name }}</option>
                                            <option ng-repeat="vehicle in vehicles" value="@{{'IrisGPS\\Vehicle-' + vehicle.id}}">@{{ 'Vehicle: ' + vehicle.plate }}</option>
                                        </select>
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-md-6 margin-bottom-20">
                                <h3 class="page-header">Puntos de visitas
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#newMarkerModal" class="btn btn-default pull-right" data-original-title="new marker" data-toggle="tooltip" data-placement="top"><i class="fa fa-plus"></i></a>
                                </h3>

                                <div class="form-group">

                                    <label for="Markers" class="control-label col-md-4">Ubicación</label>

                                    <div class="col-md-6">
                                        <select class="form-control" name="marker" ng-model="selectedMarker">
                                            <option ng-repeat="marker in markers" value="@{{marker.id}}">@{{marker.name}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="text-center ">
                                    <a href="javascript:void(0)" class="btn btn-success" ng-click="addMarkerToList()">Agregar ubicación a lista de visitas</a>
                                </div>
                                <br><br>
                                <div class="row margin-bottom-20">
                                    <div class="col-md-12">
                                        <h4 class="page-header" style="font-size: 30px;">Lista de puntos de visitas</h4>
                                        <!-- <div class="description-row">
                                            <label>
                                                Inicia:
                                            </label>
                                            <p>@{{ task.start_date }}</p>
                                        </div>
                                        <div class="description-row">
                                            <label>
                                                Finaliza:
                                            </label>
                                            <p>@{{ task.end_date }}</p>
                                        </div>
                                        <div class="description-row">
                                            <label>
                                                Descripción:
                                            </label>
                                            <p>@{{ task.description }}</p>
                                        </div> -->
                                        <div ng-hide="task.visit_list.length" class="description-row">
                                            <!-- <label>
                                                Lista de visitas:
                                            </label> -->
                                            <p>No hay ubicaciones agregadas a la lista de tareas</p>
                                        </div>
                                        <table ng-show="task.visit_list.length" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="text-center">acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="visit-list"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary" ng-disabled="taskForm.$invalid || isSubmitted">
                                    <i class="fa fa-btn fa-check"></i>Crear nueva ruta
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include('web.includes.google-maps')
    {{ Html::script('assets/irisgps/js/maps.js') }}
    {{ Html::script('assets/irisgps/angular-app/angular.irisgps.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/MarkerService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/AgentService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/VehicleService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/TaskService.js') }}
    <script>


        var map;
        map = initMap('map');
        var mapNewMarker;
        var markers = [];
        var selectedMarkers = [];
        setMapStyles(map, defaultMapStyle());
        map.setZoom(12);

        irisGpsApp.controller('taskController', ['$scope', '$compile', '$window', 'MarkerService', 'AgentService', 'VehicleService', 'TaskService', function($scope, $compile, $window, MarkerService, AgentService, VehicleService, TaskService) {

            $scope.isSubmitted = false;

            $scope.task = {
                start_date: null,
                end_date: null,
                description: null,
                assigned_to: null,
                visit_list: selectedMarkers
            };


            $('#start_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm'
            });

            $('#end_date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm',
                useCurrent: false
            });

            $('#start_date').on('dp.change', function (event) {
                $('#end_date').data("DateTimePicker").minDate(event.date);
                $scope.task.start_date = $('#start_date').val();
                $scope.$apply();
            }).focusout(function (e) {
                $scope.task.start_date = $('#start_date').val();
                $scope.$apply();
            });

            $('#end_date').on('dp.change', function (event) {
                $('#start_date').data("DateTimePicker").maxDate(event.date);
                $scope.task.end_date = $('#end_date').val();
                $scope.$apply();
            }).focusout(function (e) {
                $scope.task.end_date = $('#end_date').val();
                $scope.$apply();
            });

            $scope.markers = {};
            $scope.agents = {};
            $scope.vehicles = {};
            $scope.selectedMarker;
            $scope.selectedChecklist = [];
            $scope.newMarker = {};

            loadMarkers();
            loadAgents();
            vehiclesAgents();

            function loadMarkers() {
                MarkerService.markers().then(function(markers){
                    $scope.markers = markers;
                    $scope.selectedMarker = '' + markers[0].id;
                });
            }

            function loadAgents() {
                AgentService.agents().then(function(agents){
                    $scope.agents = agents;
                    $scope.task.assigned_to = 'IrisGPS\\Agent-' + agents[0].id;
                });
            }

            function vehiclesAgents() {
                VehicleService.vehicles().then(function(vehicles){
                    $scope.vehicles = vehicles;
                    $scope.task.assigned_to = 'IrisGPS\\Vehicle-' + vehicles[0].id;
                });
            }

            function htmlMarker(markerData) {
                return '<div id="marker_id_' + markerData.id + '" data-role="marker" data-lat="' + markerData.lat +'" data-lng="' + markerData.lng + '" data-title="' + markerData.title + '" data-icon="' + markerData.icon +'" data-info-window="' + markerData.infoWindow + '"></div>';
            }

            function htmlVisitItem(item) {
                return '<tr id="visit_point_id_' + item.id + '">' +
                        '<td>' + item.title + '</td>' +
                        '<td class="text-center">' +
                        '<a href="javascript:void(0)" class="btn btn-xs btn-primary margin-right-5" data-original-title="checklist" data-toggle="tooltip" data-placement="top" ng-click="displayChecklist(' + item.id + ')"><span class="fa fa-tasks" aria-hidden="true"></span></a>' +
                        '<a href="javascript:void(0)" class="btn btn-xs btn-danger margin-right-5" data-original-title="remove" data-toggle="tooltip" data-placement="top" ng-click="removeMarkerFromList(' + item.id + ')"><span class="fa fa-trash" aria-hidden="true"></span></a>' +
                        '</td>' +
                        '</tr>';
            }

            function findMarkerById(id) {
                var result = $.grep($scope.markers, function(e){ return e.id == id; });
                return result[0];
            }

            function findSelectedMarkerIndexById(id) {
                for (var i = 0; i < selectedMarkers.length; i++) {
                    if (selectedMarkers[i].id == id) {
                        return i;
                    }
                }
                return null;
            }

            $('#checklistModal').on('shown.bs.modal', function () {
                //$('#myInput').focus()
            })

            $scope.displayChecklist = function(id) {
                var selectedMarker = findMarkerById(id);
                $scope.selectedChecklist = selectedMarker;
                $('#checklistModal').modal('show');
            }

            $scope.closeChecklist = function() {
                $('#checklistModal').modal('hide');
            }

            $scope.removeItemFromChecklist = function(index) {
                $scope.selectedChecklist.checklist.splice(index, 1);
            }

            $scope.addItemToChecklist = function(id) {
                var selectedMarker = findMarkerById(id);
                selectedMarker.checklist.push($scope.newItem);
                $scope.selectedChecklist = selectedMarker;
                $scope.newItem = null;
            }

            $scope.addMarkerToList = function() {

                var selectedMarker = findMarkerById($scope.selectedMarker);

                if ( findSelectedMarkerIndexById($scope.selectedMarker) == null ) {
                    var markerData = {};
                    markerData.id = selectedMarker.id;
                    markerData.lat = parseFloat(selectedMarker.lat);
                    markerData.lng = parseFloat(selectedMarker.lng);
                    markerData.title = selectedMarker.name;
                    markerData.icon = 'markers/scheduled.png';
                    markerData.infoWindow = '<strong>' + selectedMarker.name + '</strong>';
                    var strHtmlMarker = htmlMarker(markerData);

                    angular.element(document.getElementById('map-markers')).append($compile(strHtmlMarker)($scope));

                    var strHtmlVisitItem = htmlVisitItem(markerData);

                    angular.element(document.getElementById('visit-list')).append($compile(strHtmlVisitItem)($scope));
                    removeMarkerFromMap(markers);
                    markers = loadMarkersFromHtml(map);
                    setMarkerOnMap(map, markers);
                    selectedMarker.checklist = [];
                    selectedMarkers.push(selectedMarker);
                    displayToastr('toast-bottom-right', 'success', 'Se agregó ubicación a la lista de visitas.');
                    $('[data-toggle="tooltip"]').tooltip();
                } else {
                    displayToastr('toast-bottom-right', 'warning', 'Esta ubicación ya fue agregada.');
                }
            };

            $scope.removeMarkerFromList = function(id) {

                var markerIndex = findSelectedMarkerIndexById(id);
                angular.element(document.getElementById('marker_id_' + id)).remove();
                angular.element(document.getElementById('visit_point_id_' + id)).remove();
                selectedMarkers.splice(markerIndex, 1);
                removeMarkerFromMap(markers);
                markers = loadMarkersFromHtml(map);
                setMarkerOnMap(map, markers);
                displayToastr('toast-bottom-right', 'success', 'Ubicación removida de la lista de visitas');
            }

            $scope.addNewMarker = function() {
                MarkerService.store($scope.newMarker).then(function(response){
                    displayToastr('toast-bottom-right', 'success', response);
                    $scope.newMarker = {};
                    loadMarkers();
                    $('#newMarkerModal').modal('hide');
                }, function(response){
                    var errors = response.data;
                    $.each(errors, function ( index, value ) {
                        displayToastr('toast-bottom-right', 'error', value);
                    });
                });
            }

            $scope.createTask = function() {
                if ($scope.task.visit_list.length) {
                    if(!validateVisitPointsChecklists()) {
                        displayToastr('toast-bottom-right', 'error', "Los puntos de visita deben tener contenido en sus listas.");
                    } else {
                        $scope.isSubmitted = true;
                        TaskService.store($scope.task).then(function (response) {
                            displayToastr('toast-bottom-right', 'success', response);
                            $scope.isSubmitted = false;
                            $window.location.replace( baseUrl + "/tasks");
                        }, function (response) {
                            var errors = response.data;
                            $scope.isSubmitted = false;
                            $.each(errors, function (index, value) {
                                displayToastr('toast-bottom-right', 'error', value);
                            });
                        });
                    }
                } else {
                    displayToastr('toast-bottom-right', 'error', 'No hay ubicaciones agregadas a la lista de visitas.');
                }
            };

            function validateVisitPointsChecklists()
            {
                var result = true;
                $.each($scope.task.visit_list, function() {
                    if(!this.checklist.length) {
                        result = false;
                    }
                });
                return result;
            };

            $('#newMarkerModal').on('shown.bs.modal', function () {
                $('#new-marker-name').focus();
                mapNewMarker = initMap('map-new-marker');
                setMapStyles(mapNewMarker, defaultMapStyle());
                mapNewMarker.setZoom(12);

                var newMarker = validateLoc(mapNewMarker);

                google.maps.event.addListener(mapNewMarker, 'click', function(event) {
                    newMarker.setPosition(event.latLng);
                    newMarker.setMap(mapNewMarker);
                    newMarker.setAnimation(google.maps.Animation.DROP);

                    $scope.newMarker.latitude = event.latLng.lat();
                    $scope.newMarker.longitude = event.latLng.lng();
                    $scope.$apply();

                });

            });

            function validateLoc(map)
            {
                var lat = $.trim($("#new-marker-lat").val());
                var lng = $.trim($("#new-marker-lng").val());
                if(lat.length>0 && lng.length>0)
                {
                    var coordinate = new google.maps.LatLng(lat, lng);
                    var marker = new google.maps.Marker({
                        position: coordinate,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                }else{
                    var marker = new google.maps.Marker({});
                }
                return marker;
            }
        }]);

    </script>
@endsection


