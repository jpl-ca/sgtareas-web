@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                {!! Html::pageHeader("Rutas") !!}
                <div>
                    <!--Begin button-->
                        <div class="dropdown pull-right">
                          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Exportar
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="{{ url ('/tasks/index.xls?page=' . $tasks->currentPage() ) }}">XLS</a></li>
                          </ul>
                        </div>
                    <!--End button-->

                    @if(auth()->user()->hasPrivilege('create-task'))
                    <div class="text-center margin-bottom-30">
                        <a href="{{ action('Web\TaskController@create') }}" class="btn btn-primary">
                            <i class="fa fa-btn fa-tasks"></i>
                            Nueva ruta
                        </a>    
                    </div>
                    @endif
                    @include('web.utils.search-form')
                    @include('web.tasks.partials.tasks-table')
                    {{ $tasks->render() }}
                </div>
            </div>
        </div>
    </div>
@endsection
