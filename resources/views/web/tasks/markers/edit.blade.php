@extends('layouts.web')

@section('head')
    {{ Html::script('assets/phone_validator/examples/js/prism.js?1456859475147') }}
    {!! Html::script('assets/angular/angular.min.js') !!}
    {!! Html::style('assets/phone_validator/build/css/intlTelInput.css?1456859475147') !!}
@endsection

@section('body-tag')
    ng-app="irisGpsApp"
@endsection


@section('content')
<style>
    footer{
        margin-top: 80px;
    }
    #phone::-webkit-input-placeholder
    {
      color:    #666666;
    }

    #phone:-moz-placeholder
    {
      color:    #666666;
    }

    #phone::-moz-placeholder
    {
      color:    #666666;
    }

    #phone:-ms-input-placeholder
    {
      color:    #666666;
    }

</style>
    <div class="container-fluid" ng-controller="markerController">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Editando Cliente
                <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a></h2>

                <div class="row margin-bottom-20">
                    <div class="col-md-6">
                        <p>
                            <strong>Click sobre el mapa para agregar la ubicación. Las coordenadas se agregarán automáticamente</strong>
                        </p>
                        <input id="marker_id" type="hidden" value="{{ $marker->id }}">
                        <form id="markerForm">                            
                            {!! Form::fhText('name', 'Nombre', null, ['id' => 'new-marker-name', 'autocomplete' => 'off', 'ng-model' => 'newMarker.name']) !!}
                            {!! Form::fhText('address', 'Dirección', null, ['autocomplete' => 'off', 'ng-model' => 'newMarker.address']) !!}
                            <!-- {!! Form::fhText('phone', 'Teléfono', null, ['autocomplete' => 'off', 'ng-model' => 'newMarker.phone']) !!} -->
                            <div class="form-group">
                                <label for="Nombre" class="control-label col-md-4">País</label>

                                <div class="col-md-6">
                                    <select class="form-control" id="selected_country" name="country"  ng-model="selected_country"
                                     ng-options="country as country.country  for country in countries track by country.cod" >
                                    </select>
                                </div>
                            </div>
                            {!! Form::fhText('phone', 'Teléfono', null, ['id' => 'phone', 'type' => 'tel', 'autocomplete' => 'off', 'placeholder' => '@{{newMarker.phone}}']) !!}
                            {!! Form::fhText('reference', 'Referencia', null, ['autocomplete' => 'off', 'ng-model' => 'newMarker.reference']) !!}
                            {!! Form::fhText('latitude', 'Latitud', null, ['id' => 'new-marker-lat', 'autocomplete' => 'off', 'readonly' => 'true', 'ng-model' => 'newMarker.latitude']) !!}
                            {!! Form::fhText('longitude', 'Longitud', null, ['id' => 'new-marker-lng', 'autocomplete' => 'off', 'readonly' => 'true', 'ng-model' => 'newMarker.longitude']) !!}

                        </form>
                    </div>
                    <div class="col-md-6">
                        <div id="map-new-marker"></div>
                    </div>
                </div>

                <div class="row margin-bottom-20">
                    <div class="col-md-12 text-center">
                        <button type="button" class="btn btn-primary" ng-disabled="markerForm.$invalid || isSubmitted" ng-click="addNewMarker()">
                            <i class="fa fa-check"></i>
                            Actualizar cliente
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('javascript')
    @include('web.includes.google-maps')
    {{ Html::script('assets/irisgps/js/maps.js') }}
    {{ Html::script('assets/phone_validator/build/js/intlTelInput.js?1456859475147') }}
    {{ Html::script('assets/phone_validator/examples/js/isValidNumber.js?1456859475147') }}
    {{ Html::script('assets/irisgps/angular-app/angular.irisgps.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/MarkerService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/AgentService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/VehicleService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/TaskService.js') }}
    <script>

        irisGpsApp.controller('markerController', ['$scope', '$compile', '$window', 'MarkerService', function($scope, $compile, $window, MarkerService) {
            $scope.loadCountries = function(){
                MarkerService.countries().then(function(response){
                    $scope.countries = response;
                })
            }
            $scope.loadCountries();
            $scope.$watch('selected_country', function(country){
                if(country){
                    $scope.centermap = {lat:country.lat, lng:country.lng};

                    $('#new-marker-name').focus();
                    mapNewMarker = initMap2('map-new-marker'); 
                    setMapStyles(mapNewMarker, defaultMapStyle());
                    // mapNewMarker.setZoom(12);

                    var newMarker = validateLoc(mapNewMarker);

                    google.maps.event.addListener(mapNewMarker, 'click', function(event) {
                    newMarker.setPosition(event.latLng);
                    newMarker.setMap(mapNewMarker);
                    newMarker.setAnimation(google.maps.Animation.DROP);

                    $scope.newMarker.latitude = event.latLng.lat();
                    $scope.newMarker.longitude = event.latLng.lng();
                    $scope.$apply();

                    });
                }               
            });

            function initMap2(elementId, mapOptions) {
                var map;
                console.log($scope.centermap);
                if (mapOptions == null) {
                    mapOptions = {
                        zoom: 5,
                        center: $scope.centermap,
                        streetViewControl: false,
                        mapTypeControl: false   ,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                }
                var map = new google.maps.Map(document.getElementById(elementId), mapOptions)
                return map;
                
            }
            
            var markerId = $('#marker_id').val();

            $scope.newMarker = {};

            getMarker();

            $scope.isSubmitted = false;

            $scope.addNewMarker = function() {
                if ($.trim(telInput.val()) == "") {
                     $scope.isSubmitted = true;
                        $scope.newMarker.id = markerId;
                        $scope.apply;
                        MarkerService.update($scope.newMarker).then(function(response){
                            displayToastr('toast-bottom-right', 'success', response);
                            $scope.newMarker = {};
                            $window.location.replace("/markers");
                        }, function(response){
                            var errors = response.data;
                            $.each(errors, function ( index, value ) {
                                displayToastr('toast-bottom-right', 'error', value);
                                $scope.isSubmitted = false;
                            });
                        });
                }else if ($.trim(telInput.val())) {
                    if (telInput.intlTelInput("isValidNumber")) {
                        $scope.isSubmitted = true;
                        $scope.newMarker.id = markerId;
                        $scope.apply;
                        // $scope.newMarker.phone = $('#phone').val();
                        MarkerService.update($scope.newMarker).then(function(response){
                            displayToastr('toast-bottom-right', 'success', response);
                            $scope.newMarker = {};
                            $window.location.replace("/markers");
                        }, function(response){
                            var errors = response.data;
                            $.each(errors, function ( index, value ) {
                                displayToastr('toast-bottom-right', 'error', value);
                                $scope.isSubmitted = false;
                            });
                        });
                    } else {
                      telInput.addClass("error");
                      displayToastr('toast-bottom-right', 'error', "Ingrese un teléfono valido");
                    }
                }
         
            }

            function validateLoc(map)
            {
                var lat = $scope.newMarker.latitude;
                var lng = $scope.newMarker.longitude;
                if(lat.length>0 && lng.length>0)
                {
                    var coordinate = new google.maps.LatLng(lat, lng);
                    var marker = new google.maps.Marker({
                        position: coordinate,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                }else{
                    var marker = new google.maps.Marker({});
                }
                return marker;
            }

            function getMarker()
            {
                MarkerService.marker(markerId).then(function (response) {
                    console.log(response);
                    $scope.newMarker = {
                        name: response.name,
                        latitude: response.lat,
                        longitude: response.lng,
                        address: response.address,
                        phone: response.phone,
                        reference: response.reference,
                    };
                    $scope.apply;
/*
                    var newMarker = validateLoc(mapNewMarker);

                    google.maps.event.addListener(mapNewMarker, 'click', function(event) {
                        newMarker.setPosition(event.latLng);
                        newMarker.setMap(mapNewMarker);
                        newMarker.setAnimation(google.maps.Animation.DROP);

                        $scope.newMarker.latitude = event.latLng.lat();
                        $scope.newMarker.longitude = event.latLng.lng();
                        $scope.$apply();

                    });
*/
                    loadMarkerOnMap({ lat: response.lat, lng: response.lng});

                }, function (response) {

                });
            };

            var loadMarkerOnMap = function (marker) {

                if (!marker || !marker.lat || !marker.lng) {
                    throw new Error("Lat and Lng properties are not found");
                }

                marker.lat = parseFloat(marker.lat);
                marker.lng = parseFloat(marker.lng);

                console.log(marker);
                var options = {
                    zoom: 8,
                    center: {lat: marker.lat, lng: marker.lng},
                    streetViewControl: false,
                    mapTypeControl: false   ,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    geolocationNavigator: false
                };

                $('#new-marker-name').focus();
                
                var mapNewMarker = initMap('map-new-marker', options);
                var newMarker = validateLoc(mapNewMarker);
                
                setMapStyles(mapNewMarker, defaultMapStyle());
                mapNewMarker.setZoom(12);
                google.maps.event.addListener(mapNewMarker, 'click', function(event) {
                        newMarker.setPosition(event.latLng);
                        newMarker.setMap(mapNewMarker);
                        newMarker.setAnimation(google.maps.Animation.DROP);

                        $scope.newMarker.latitude = event.latLng.lat();
                        $scope.newMarker.longitude = event.latLng.lng();
                        $scope.$apply();

                    });
            }

        }]);

    </script>
@endsection