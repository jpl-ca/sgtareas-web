@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid">
        <div class="container  margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Clientes</h2>
                <div>
                    
                    <nav class="navbar navbar-default">
                    <!--Begin button-->
                        <div class="dropdown pull-right margin-top-15 margin-right-15">
                          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Exportar
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="{{ url ('/markers/index.xls?page=' . $markers->currentPage() ) }}">XLS</a></li>
                          </ul>
                        </div>
                        <!--End button-->
                      <div class="container">
                        <form class="navbar-form navbar-left" role="search" method='GET' action='{{Request::url()}}'>
                            <div class="form-group">
                                {{ Form::text('search_text', (isset($searchText) ? $searchText : ''), ['class' => 'form-control', 'placeholder' => 'Ingrese texto a buscar']) }}
                            </div>
                            <div class='form-group'>
                                {{ Form::select('search_type', (isset($searchTypes) ? $searchTypes : []), (isset($searchType) ? $searchType : null), ['class' => 'form-control']) }}
                            </div>
                            <button type="submit" class="btn btn-default">Buscar</button>
                        </form>
                        
                        @if($hasPrivilegesToCreate)
                        <a href="{{ action('Web\MarkerController@create') }}" class="btn btn-primary  margin-top-15 margin-right-15 pull-right">
                            <i class="fa fa-btn fa-map-marker"></i>
                            Nuevo cliente
                        </a>
                        @endif
                      </div>
                    </nav>
                    @include('web.tasks.markers.partials.markers-table')
                    {{ $markers->render() }}
                </div>
            </div>
        </div>
    </div>
@endsection
