{!! Form::fhText('name', 'Nombre', null, ['id' => 'new-marker-name', 'autocomplete' => 'off', 'ng-model' => 'newMarker.name']) !!}
{!! Form::fhText('address', 'Dirección', null, ['autocomplete' => 'off', 'ng-model' => 'newMarker.address']) !!}
<!-- {!! Form::fhText('phone', 'Teléfono', null, ['autocomplete' => 'off', 'ng-model' => 'newMarker.phone']) !!} -->
<div class="form-group">
    <label for="Nombre" class="control-label col-md-4">País</label>

    <div class="col-md-6">
    	<select class="form-control" id="selected_country" name="country"  ng-model="selected_country"
         ng-options="country as country.country  for country in countries track by country.cod" >
        </select>
    </div>
</div>
{!! Form::fhText('phone', 'Teléfono', null, ['id' => 'phone', 'type' => 'tel', 'autocomplete' => 'off', 'ng-model' => 'newMarker.phone']) !!}
<!-- <span id="valid-msg" class="hide">✓ Valido</span>
<span id="error-msg" class="hide">Número Valido</span> -->
{!! Form::fhText('reference', 'Referencia', null, ['autocomplete' => 'off', 'ng-model' => 'newMarker.reference']) !!}
{!! Form::fhText('latitude', 'Latitud', null, ['id' => 'new-marker-lat', 'autocomplete' => 'off', 'readonly' => 'true', 'ng-model' => 'newMarker.latitude']) !!}
{!! Form::fhText('longitude', 'Longitud', null, ['id' => 'new-marker-lng', 'autocomplete' => 'off', 'readonly' => 'true', 'ng-model' => 'newMarker.longitude']) !!}
