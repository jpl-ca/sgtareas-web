<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Teléfono</th>
        <th>Referencia</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @forelse($markers as $marker)
        <tr>
            <th>{{ $marker->id  }}</th>
            <td>{{ $marker->name }}</td>
            <td>{{ $marker->address }}</td>
            <td>{{ $marker->phone }}</td>
            <td>{{ $marker->reference }}</td>
            <td class="text-center">
                @if($hasPrivilegesToView)
                <a data-original-title="ver" href="{{ action('Web\MarkerController@view', $marker) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>
                @endif

                @if($hasPrivilegesToEdit)
                <a data-original-title="editar" href="{{ action('Web\MarkerController@edit', $marker) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>
                @endif

                @if($hasPrivilegesToDestroy)
                <a data-original-title="eliminar"
                   href="{{ action('Web\MarkerController@destroy', $marker) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de eliminar esto?"
                   data-confirm-button="Sí, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-trash" aria-hidden="true"></span>
                </a>
                @endif
            </td>
        </tr>
    @empty
        <tr class="text-center">
            <td colspan="6">No hay registros</td>
        </tr>
    @endforelse
    </tbody>
</table>