<div class="row margin-bottom-20">
    <div class="col-md-12">
        {!! Form::fhText('description', 'Descripción', $task->description, ['readonly' => true]) !!}

        {!! Form::fhText('start_date', 'Fecha de inicio', $task->start_date, ['readonly' => true]) !!}

        {!! Form::fhText('end_date', 'Fecha fin', $task->end_date, ['readonly' => true]) !!}

    </div>
</div>