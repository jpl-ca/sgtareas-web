@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Nuevo Agente
                <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a></h2>

                {!! Form::fhOpen(['action' => 'Web\TrackableController@storeAgent', 'method' => 'POST']) !!}

                    {!! Form::token() !!}

                    {!! Form::fhText('first_name', 'Nombres', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('last_name', 'Apellidos', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('authentication_code', 'Código de autenticación', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhPassword('password', 'Clave') !!}

                    {!! Form::fhSubmit('Crear nuevo agente', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
