@extends('layouts.web')

@section('content')
<style>
    footer{margin-top:83px;}
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Agente #{{$agent->authentication_code}}
                <a href="{{ action('Web\TrackableController@index') }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a></h2>

                <div class="row margin-bottom-20">
                    <div class="col-md-12">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Información</a></li>
                            <li role="presentation"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab">Rutas</a></li>
                            <li role="presentation"><a href="#geolocation" aria-controls="geolocation" role="tab" data-toggle="tab">Geolocalización</a></li>
                        </ul>

                    </div>
                </div>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="info">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include('web.trackables.agents.partials.information')
                                <a href="{{ action('Web\TrackableController@editAgent', $agent) }}" class="btn btn-primary margin-bottom-20">Editar</a>

                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tasks">
                        <div class="row">
                            <div class="col-md-12">
                                @include('web.tasks.partials.tasks-table')
                            </div>
                        </div>
                    </div>
                        <div role="tabpanel" class="tab-pane" id="geolocation">
                            <div class="row">
                                <div class="col-md-12">
                                    @include('web.trackables.agents.partials.geolocation')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include('web.includes.google-maps')
    {!! Html::script('assets/irisgps/js/maps.js') !!}
    {!! Html::script('assets/irisgps/js/geolocation.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('a[data-toggle="tab"][href="#geolocation"]').on('shown.bs.tab', function (e) {
                var lat = $("#map").data("lat");
                var lng = $("#map").data("lng");
                if (lat && lng) {
                    var mapOptions = {
                        zoom: 15,
                        center: {lat: lat, lng: lng},
                        streetViewControl: false,
                        mapTypeControl: false   ,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    };

                    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
                    var marker = new google.maps.Marker({
                      position: {lat: lat, lng: lng},
                      map: map,
                    });
                }
            });
        });
    </script>
@endsection
