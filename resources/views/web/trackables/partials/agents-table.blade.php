<table class="table">
    <thead>
        <tr>
            <th>Código</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Estado</th>
            <th class="text-center">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @forelse($agents as $agent)
        <tr>
            <th>{{ $agent->authentication_code  }}</th>
            <td>{{ $agent->first_name }}</td>
            <td>{{ $agent->last_name }}</td>
            <td>
                @if($agent->trackable->isConnected())
                    <span id="label_{{ $agent->trackable->trackable_type . '_' . $agent->id }}" class="label label-{{ $agent->trackable->state == \IrisGPS\Trackable::STATE_ACTIVE ? 'success' : 'warning' }}">{{ $agent->trackable->state }}</span>
                    <a id="disconnect_{{ $agent->trackable->trackable_type . '_' . $agent->id }}" data-original-title="disconnect" href="javascript:void(0)" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" ng-click="disconnect('{{ $agent->type }}', '{{ $agent->authentication_code }}', '{{ $agent->id }}', '{{ addslashes($agent->trackable->trackable_type) }}')">
                        <span class="fa fa-power-off" aria-hidden="true"></span>
                    </a>
                @else
                    <span class="label label-default">desconectado</span>
                @endif
            </td>
            <td class="text-center">
                <a data-original-title="ver historial de ubicaciones" href="{{ action('Web\TrackableController@geolocationHistoryAgent', $agent->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-map-marker" aria-hidden="true"></span>
                </a>
                @if(auth()->user()->hasPrivilege('view-trackable-agent'))
                <a data-original-title="ver" href="{{ action('Web\TrackableController@viewAgent', $agent->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>
                @endif
                @if(auth()->user()->hasPrivilege('edit-trackable-agent'))
                <a data-original-title="editar" href="{{ action('Web\TrackableController@editAgent', $agent->id) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>
                @endif
                @if(auth()->user()->hasPrivilege('destroy-trackable-agent'))
                <a data-original-title="eliminar"
                   href="{{ action('Web\TrackableController@destroy', $agent->trackable) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de eliminar este agente?"
                   data-confirm-button="Si, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-trash" aria-hidden="true"></span>
                </a>
                @endif
            </td>
        </tr>
    @empty
        <tr class="text-center">
            <td colspan="5">No hay registros</td>
        </tr>
    @endforelse
    </tbody>
</table>