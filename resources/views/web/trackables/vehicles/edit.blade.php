@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
                {!! Html::pageHeader("Edición de Vehículo #$vehicle->plate") !!}

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ url()->previous() }}" class="btn btn-default pull-left margin-bottom-20">Ir atrás</a>
                    </div>
                </div>

                {!! Form::fhOpen(['action' => ['Web\TrackableController@updateVehicle', $vehicle], 'method' => 'POST']) !!}

                {!! Form::token() !!}

                {!! Form::fhText('plate', 'Placa', $vehicle->plate, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('brand', 'Marca', $vehicle->brand, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('model', 'Modelo', $vehicle->model, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('color', 'Color', $vehicle->color, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('manufacture_year', 'Año de fabricación', $vehicle->manufacture_year, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('gas_consumption_rate', 'Tarifa de consumo de gas', $vehicle->gas_consumption_rate, ['autocomplete' => 'off']) !!}

                {!! Form::fhPassword('password', 'Clave de autenticación') !!}

                {!! Form::fhSubmit('Actualización de vehículo', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
