@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Edición de usuario
                <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>


                {!! Form::fhOpen(['action' => ['Web\UserController@update', $user], 'method' => 'POST']) !!}

                {!! Form::token() !!}
                
                @if(isset($nextUrl) && $nextUrl)
                {!! Form::hidden('nextUrl', $nextUrl) !!}
                @endif

                {!! Form::fhText('name', 'Nombre', $user->name, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('email', 'Correo', $user->email, ['autocomplete' => 'off']) !!}
                
                {!! Form::fhPassword('password', 'Contraseña') !!}
                
                <div class='form-group'>
                    <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Privilegios</h2>

                </div>
                <div class="form-group">
                    <label class='control-label col-md-4'>Seleccionar Todo</label>
                    <div>
                        <input type="checkbox" id="checkAll"/>
                    </div>
                </div>
                @foreach($privileges as $privilege)
                <div class='form-group'>
                    <label class='control-label col-md-4'>
                        {{$privilege->name}}
                    </label>
                    <div>
                        {{Form::checkbox("privileges[]", $privilege->id, $user->hasPrivilege($privilege->id))}}
                    </div>

                </div>                
                @endforeach
                                
                {!! Form::fhSubmit('Actualizar usuario', 'primary', 'check') !!}

                {!! Form::fhClose() !!}


            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script>
    $("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
</script>
@endsection