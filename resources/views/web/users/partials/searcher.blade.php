<nav class="navbar navbar-default">
  <div class="container">
    <form class="navbar-form navbar-left" role="search" method='GET'>
        <div class="form-group">
          	{{ Form::text('search_text', $searchText, ['class' => 'form-control']) }}
        </div>
        <div class='form-group'>
        	{{ Form::select('search_type', $searchTypes, $searchType, ['class' => 'form-control']) }}
        </div>
        <button type="submit" class="btn btn-default">Buscar</button>
    </form>
  </div>
</nav>